// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
// library and dependencies
//= require jquery
//= require jquery_ujs
//= require angular
//= require angular-route
//= require angular-animate
//= require angular-aria
//= require angular-cookies
//= require angular-messages
//= require angular-sanitize
//= require angular-touch
//= require angular-loading-bar
//= require ng-notifications-bar
//= require angular-validation-match
//= require angular-rails-templates
//= require bootstrap
//= require qrcode
//= require angular-qr
//= require angular-ui-utils

// angular files

//= require bitcoin-gateway/app
//= require bitcoin-gateway/helper
//= require_tree ./bitcoin-gateway/templates
//= require_tree ./bitcoin-gateway/modules
//= require_tree ./bitcoin-gateway/filters
//= require_tree ./bitcoin-gateway/directives
//= require_tree ./bitcoin-gateway/models
//= require_tree ./bitcoin-gateway/services
//= require_tree ./bitcoin-gateway/controllers