'use strict';

/*
  Developed by: Sneha Wangle
  Created at: 19-May-2015
  Purpose: To render about us page
*/

window.app
  .controller('AboutUsController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    // To highlight home link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.about;
  }]);
