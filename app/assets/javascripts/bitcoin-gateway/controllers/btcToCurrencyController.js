'use strict';

/*
  Developed by: Sneha Wangle
  Created at: 10-June-2015
  Purpose: To show the Convert BTC To Currency Page functionality
*/

window.app
  .controller('BtcToCurrencyController', ['$scope', '$rootScope', '$location', 'Notify', 'Account', 'currencyRate', function ($scope, $rootScope, $location, Notify, Account, currencyRate) {

    $scope.currency = "USD";
    
    // To enable convert btn
    $scope.enableConvertBtn = true;

    // Listening logout btn click event
    $scope.$on('AuthService:signedOut', function(){
      // Must be redirect to home path after logout
      $location.path('/')
    });
    

    // Success handler for merchant convert
    var bankDetailsSuccessHandler = function(response) {
      // Must be enable convert button
      $scope.enableConvertBtn = true;
      Notify.success('Bank Account Details Saved Successfully.', 4000);
      // Redirect to resend confirmation page.
      $location.path('/profile');
    };

    // Error handler for merchant convert
    var bankDetailsErrorHandler = function(response) {
      // Must be enable convert button
      $scope.enableConvertBtn = true;
      if (response.statusCode == 422) 
        Notify.error('Form validation fail.', 4000);
      else
        Notify.error(response.data.error, 4000);
    };

    // Purpose: To fetch current rate of BTC from api call 
    $scope.currency_rate = eval('currencyRate.data.'+$scope.currency).last;

    // Merchant bank_accounts request maker
    $scope.merchantBankDetails = function() {
      // To ensure, btcToUsd form is valid.
      if (this.currencyConversion.$valid) {
        // Must be disable convert button
        $scope.enableConvertBtn = false;
        // Preparing input parameters for bank account api call
        var postData = {
                          account_number: $scope.account_number,
                          bank_name: $scope.bank_name,
                          country: $scope.country,
                          city: $scope.city,
                          bank_code: $scope.bank_code,
                          swift_code: $scope.swift_code,
                          address: $scope.address,
                          btc_amount: $scope.amount,
                          currency: $scope.currency,
                          currency_rate: $scope.currency_rate
                        };
        // Making API call for merchant bank_accounts.
        Account.bank_accounts(postData).then(bankDetailsSuccessHandler, bankDetailsErrorHandler);
      }
      else{
        // currencyConversion form pristine must be false to show validation messsages
        this.currencyConversion.$pristine = false;

        // All field must be dirty who are required, to enable error message below field.
        angular.forEach(this.currencyConversion.$error.required, function(field) {
          field.$setDirty();
        });
        // All field must be dirty who are required, to enable error message below field.
        setFieldsDirty(this.currencyConversion, ['terms'], true);
        // Must be enable convert button
        $scope.enableConvertBtn = true;
      };
    };
    
  }]);



