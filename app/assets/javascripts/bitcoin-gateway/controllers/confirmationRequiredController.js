'use strict';

/*
  Developed by: Divyansh Kumar 
  Created at: 04-May-2015
  Purpose: To control resend email view
*/

window.app
  .controller('ConfirmationRequiredController', ['$scope', '$rootScope', 'Session', '$location', 'Notify', 'Merchant', function ($scope, $rootScope, Session, $location, Notify, Merchant) {
    // To highlight signup link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.signup;
    // To enable resend confirmation btn
    $scope.enableResendConfirmationBtn = true;

    // pick email from browser session or set blank
    $scope.email = Session.get('confirmationEmail') == undefined ? "" : Session.get('confirmationEmail');

    // Success handler for resend verification mail API call
    var resendSuccessHandler = function(response) {
      Notify.success("Email Sent to "+response.data.email, 4000);
      // Must be enabled resend confirmation btn
      $scope.enableResendConfirmationBtn = true;
    };

    // Error handler for resend verification mail API call
    var resendErrorHandler = function(response) {
      // 422 = email alreay verified, 404 = email address doesnot exist. In both cases confirmationEmail must be clear and reidrect to signin page
      if(response.statusCode == 422 || response.statusCode == 404)
        Notify.error(response.data.error, 4000);
      else
        Notify.error(response.data.error, 4000);
      // Must be enabled resend confirmation btn
      $scope.enableResendConfirmationBtn = true;
    };

    // To make resend email verification mail
    $scope.resend = function() {
      // Must check meail address valid or not, to resend email verification mail
      if (this.resendEmail.$valid){
        // Must be disabled resend confirmation btn
        $scope.enableResendConfirmationBtn = false;
        Merchant.resendConfirmation($scope.email).then(resendSuccessHandler, resendErrorHandler);
      }
      else{
        // resendEmail form peristine must be false to show validation messsages
        this.resendEmail.$pristine = false;
        // All field must be dirty who are required, to enable error message below field.
        angular.forEach(this.resendEmail.$error.required, function(field) {
          field.$setDirty();
        });
      }
    };
  }]);
