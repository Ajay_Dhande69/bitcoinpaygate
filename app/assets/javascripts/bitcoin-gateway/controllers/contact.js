'use strict';

/*
  Developed by: Sneha Wangle
  Created at: 20-May-2015
  Purpose: To render contact us page
*/

window.app
  .controller('ContactUsController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    // To highlight home link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.contact;
  }]);
