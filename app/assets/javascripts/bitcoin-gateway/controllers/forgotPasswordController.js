'use strict';

/*
  Developed by: Divyansh kumar
  Created at: 11-May-2015
  Purpose: To control forgot password fucntionality
*/

window.app
  .controller('ForgotPasswordController', ['$scope', '$rootScope', '$location', 'Notify', 'Merchant', function ($scope, $rootScope, $location, Notify, Merchant) {
    // To highlight signin link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.signin;

    // Must be enable reset button
    $scope.enableResetBtn = true;

    // Success handler for merchant forget password API call
    var forgetPasswordSuccesshandler = function(response) {
      Notify.success('Reset password link sent successfully', 4000);
      $location.path('/');
      // Must be enable reset button
      $scope.enableResetBtn = true;
    };

    // Error handler for merchant forget password API call
    var forgetPasswordErrorhandler = function(response) {
      if(response.statusCode == 422)
        Notify.error('Email doesn\'t exist', 4000);
      else
        Notify.error('Unable to send reset password link', 4000);
      // Must be enable reset button
      $scope.enableResetBtn = true;
    };

    //Calling API to send reset password link to email address
    $scope.forgetPassword = function() {
      // To ensure, forget password form is valid.
      if (this.forgotPassword.$valid) {
        // Must be disable reset button
        $scope.enableResetBtn = false;
        // Making API call to send reset password link to merchant email address.
        Merchant.sendResetPasswordLink($scope.email).then(forgetPasswordSuccesshandler, forgetPasswordErrorhandler);
      }
      else{
        // Signup form peristine must be false to show validation messsages
        this.forgotPassword.$pristine = false;
        // All field must be dirty who are required, to enable error message below field.
        angular.forEach(this.forgotPassword.$error.required, function(field) {
          field.$setDirty();
        });
        // Must be enable reset button
        $scope.enableResetBtn = true;
      };
    };

  }]);
