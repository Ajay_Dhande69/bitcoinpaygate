'use strict';

/*
  Developed by: Divyansh Kumar
  Created at: 11-May-2015
  Purpose: To control header
*/

window.app
  .controller('HeaderController', ['$scope', '$rootScope', 'Notify', 'AuthService', function ($scope, $rootScope, Notify, AuthService) {

    // Listening logout event
    $scope.$on('AuthService:signedOut', function(){
      // Must be set true on logout
      $rootScope.signedIn = false;
    });

    // Listening signed in event
    $scope.$on('AuthService:signedIn', function(){
      // Must be set true on successful login
      $rootScope.signedIn = true;
    });

    $scope.signOut = function() {
      AuthService.signOut();
      Notify.success('Logout Successfully', 4000);
    };

    $scope.toggleNav = function() {
      var elem = angular.element('button[data-target=".navbar-collapse"]');
      if (elem.css("display") != "none")
        angular.element('button[data-target=".navbar-collapse"]').click();
    };

  }]);
