'use strict';

/*
  Developed by: Divyansh Kumar 
  Created at: 02-May-2015
  Purpose: To render root page
*/

window.app
  .controller('MainController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    // To highlight home link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.home;
  }]);
