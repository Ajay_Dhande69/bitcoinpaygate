'use strict';

/*
  Developed by: Sneha Wangle
  Created at: 08-May-2015
  Purpose: To show the profile page functionality
*/

window.app
  .controller('ProfileController', ['$scope', '$rootScope', '$location', 'Notify', 'Profile', 'tx_details', 'accountDetails', function ($scope, $rootScope, $location, Notify, Profile, tx_details, accountDetails) {
    // To highlight signin link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.profile;

    // Must enable amount refresh btn
    $scope.enableAmountRefreshBtn = true;

    // Listening logout btn click event
    $scope.$on('AuthService:signedOut', function(){
      // Must be redirect to home path after logout
      $location.path('/')
    });
    
    //Redirect to convert page
    $scope.select = function() {
      $location.path('/convert')
    }
    
    // Success or error handler for total amount api call
    var totalAmountResponseHandler = function(response) {
      // Must enable amount refresh btn
      $scope.enableAmountRefreshBtn = true;
      $scope.total_amount = response;
    };

    $scope.getTotalAmount = function () {
      // Purpose: To show total amount in merchant wallet
      $scope.total_amount = 'Please Wait...';
      // Must disable amount refresh btn
      $scope.enableAmountRefreshBtn = false;
      Profile.total_amount().then(totalAmountResponseHandler, totalAmountResponseHandler);
      Profile.transaction_details().then(function(response) {
        $scope.transactions = response.transactions;
      });
    };

    // Calling total amount api
    $scope.getTotalAmount();
    
    // Purpose: To show transactions(last 10 transactions) details
    $scope.transactions = tx_details.transactions;

    // set api key and secret key
    $scope.apiToken = accountDetails.data.user.api_token;
    $scope.secretKey = accountDetails.data.user.secret_key;

    // tab onlick event listener to update location search paramsters
    $scope.switchTab = function(event) {
      event.preventDefault();
      // Update search params
      $location.search('tabID', event.currentTarget.hash.slice(1));
    };

    // logic to render tab1, initially
    var renderFirstTab = function () {
      // To get location search paramsters
      var searchParams = $location.search();
      // Check tabID is undefined or check tabID value is invalid
      if (searchParams.tabID == undefined || jQuery.inArray(searchParams.tabID, ['tab1', 'tab2', 'tab3']) == -1)
        // Update search parameter to listen by locationchange listener.
        $location.search('tabID', 'tab1');
      else
        //switch tab if tabID is valid.
        jQuery('#myTab a[href="#'+searchParams.tabID+'"]').tab('show');
    };

    // location change listener to show tab according to search paramsters
    $scope.$on('$locationChangeSuccess', function(){
      // To get location search paramsters
      var searchParams = $location.search();
      // Check tabID value is valid
      if (searchParams.tabID != undefined && jQuery.inArray(searchParams.tabID, ['tab1', 'tab2', 'tab3']) != -1) {
        //switch tab
        jQuery('#myTab a[href="#'+searchParams.tabID+'"]').tab('show');
      };
    });

    $scope.toggleType = function(event) {
      var elem = jQuery(event.currentTarget);
      if (elem.html() == "Show") {
        jQuery('input[ng-model="secretKey"]').attr('type', 'text');
        elem.html('Hide');
      }
      else{
        jQuery('input[ng-model="secretKey"]').attr('type', 'password');
        elem.html('Show');
      };
    };
    
    
    // Must call to set search params initially
    renderFirstTab();

  }]);
