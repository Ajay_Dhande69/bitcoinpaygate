'use strict';

/*
  Developed by: Divyansh Kumar 
  Created at: 15-May-2015
  Purpose: To control payment receive process
*/

window.app
  .controller('ReceivePaymentController', ['$scope', '$rootScope', 'transaction', '$location', 'Transaction', function ($scope, $rootScope, transaction, $location, Transaction) {
    // To highlight signup link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.none;

    $scope.enableBtns = true;

    if (transaction.statusCode != 200) {
      $location.path('/');
      return;
    };

    $scope.transaction = transaction.data.transaction;
    $scope.qr_code = encodeURI("bitcoin:"+$scope.transaction.to_address+"?amount="+$scope.transaction.amount_in_btc+"&label="+$rootScope.qr_label);

    // tab onlick event listener to update location search paramsters
    $scope.switchTab = function(event) {
      event.preventDefault();
      // Update search params
      jQuery(event.currentTarget).tab('show');
    };

    var paymentCancelHandler = function(response) {
      window.location.href = response.data.redirect_to;
    };

    var QRSubmitHandler = function(response) {
      window.location.href = response.data.redirect_to;
    };

    var GUIDSubmitSuccessHandler = function(response) {
      window.location.href = response.data.redirect_to;
    };

    var GUIDformatter = function (guid) {
      var GUID = [];
      GUID.push(guid.substring(0, 8));
      GUID.push(guid.substring(8, 12));
      GUID.push(guid.substring(12, 16));
      GUID.push(guid.substring(16, 20));
      GUID.push(guid.substring(20, guid.length));
      return GUID.join("-");
    };

    $scope.cancel = function() {
      $scope.enableBtns = false;
      Transaction.paymentCancel($scope.transaction.transaction_id).then(paymentCancelHandler, paymentCancelHandler);
    };

    $scope.QRSubmit = function() {
      $scope.enableBtns = false;
      Transaction.QRTransaction($scope.transaction.transaction_id).then(QRSubmitHandler, QRSubmitHandler);
    };

    $scope.GUIDSubmit = function() {
      if (this.GUIDform.$valid) {
        $scope.enableBtns = false;
        var GUIDDetails = {guid: GUIDformatter($scope.guid), main_password: $scope.primary_password, second_password: $scope.secondary_password};
        Transaction.GUIDTransaction($scope.transaction.transaction_id, GUIDDetails).then(GUIDSubmitSuccessHandler, GUIDSubmitSuccessHandler);
      }
      else{
        // GUIDform form peristine must be false to show validation messsages
        this.GUIDform.$pristine = false;
        // All field must be dirty who are required, to enable error message below field.
        angular.forEach(this.GUIDform.$error.required, function(field) {
          field.$setDirty();
        });
        $scope.enableBtns = true;
      }
    };
  }]);
