'use strict';

/*
  Developed by: Divyansh kumar
  Created at: 12-May-2015
  Purpose: To control reset new password fucntionality
*/

window.app
  .controller('ResetPasswordController', ['$scope', '$rootScope', '$location', 'Notify', 'Merchant', '$routeParams', function ($scope, $rootScope, $location, Notify, Merchant, $routeParams) {
    // To highlight signin link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.signin;

    // Must be enable reset button
    $scope.enableChangeBtn = true;

    // Success handler for merchant reset password API call
    var resetPasswordSuccesshandler = function(response) {
      Notify.success('Reset password successfully', 4000);
      $location.path('/signin');
      // Must be enable reset button
      $scope.enableChangeBtn = true;
    };

    // Error handler for merchant reset password API call
    var resetPasswordErrorhandler = function(response) {
      if (response.statusCode == 422)
        Notify.error('Invalid reset password token', 4000);
      else if (response.statusCode == 401)
        Notify.error('You need to verify email first', 4000);
      else
        Notify.error("Unable to reset password", 4000);
      // Must be enable reset button
      $scope.enableChangeBtn = true;
    };

    //Calling API to reset password
    $scope.resetPass = function() {
      // To ensure, forget password form is valid.
      if (this.resetPassword.$valid) {
        // Must be disable reset button
        $scope.enableChangeBtn = false;
        $scope.user.reset_password_token = $routeParams.token;
        var postData = {user: $scope.user}
        // Making API call to reset password for merchant.
        Merchant.resetPassword(postData).then(resetPasswordSuccesshandler, resetPasswordErrorhandler);
      }
      else{
        // Signup form peristine must be false to show validation messsages
        this.resetPassword.$pristine = false;
        // All field must be dirty who are required, to enable error message below field.
        angular.forEach(this.resetPassword.$error.required, function(field) {
          field.$setDirty();
        });
        // Must be enable reset button
        $scope.enableChangeBtn = true;
      };
    };

  }]);
