'use strict';

/*
  Developed by: Sneha Wangle
  Created at: 19-May-2015
  Purpose: To render services page
*/

window.app
  .controller('ServicesController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    // To highlight home link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.services;
  }]);
