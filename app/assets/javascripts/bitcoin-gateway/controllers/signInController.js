'use strict';

/*
  Developed by: Divyansh Kumar 
  Created at: 04-May-2015
  Purpose: To control signin functionality
*/

window.app
  .controller('SignInController', ['$scope', '$rootScope', '$routeParams', 'Notify', 'AuthService', '$location', function ($scope, $rootScope, $routeParams, Notify, AuthService, $location) {
    // To highlight signin link in navigation bar.
    $rootScope.selectedNavBar = $rootScope.navBar.signin;
    // To enable sign in button
    $scope.enableSignInBtn = true;

    // To Show notification message according to notify parameter in url.
    switch($routeParams.notify) {
      // To show email verified.
      case 'confirmed':
        Notify.success('Email verified', 4000);
        AuthService.destroyAuthHeader();
        break;
      // To show email verification un-successful.
      case 'unconfirmed':
        AuthService.destroyAuthHeader();
        Notify.error('Invalid confirmation token', 4000);
        break;
    }

    // Error handler for merchant sign in
    var signInErrorHandler = function(response) {
      Notify.error(response.data.error, 4000);
      // Must be enable sign in button
      $scope.enableSignInBtn = true;
    };

    // Success handler for merchant sign in
    var signInSuccessHandler = function(response) {
      Notify.success('Signed-in Successful.', 4000);
      // Redirect to merchant profile page.
      $location.path('/profile');
    };

    // Merchant sign in request maker
    $scope.merchantLogin = function() {
      // To ensure, signin form is valid.
      if (this.signin.$valid) {
        // Must be disable sign in button
        $scope.enableSignInBtn = false;
        // Preparing input parameters for merchant sign in api call
        var postData = {
                          user: {
                            email: $scope.email,
                            password: $scope.password,
                          }
                        };
        // Making API call for merchant sign in.
        AuthService.login(postData).then(signInSuccessHandler, signInErrorHandler);
      }
      else{
        // Signup form peristine must be false to show validation messsages
        this.signin.$pristine = false;
        // All field must be dirty who are required, to enable error message below field.
        angular.forEach(this.signin.$error.required, function(field) {
          field.$setDirty();
        });
      };
    };

  }]);
