'use strict';

/*
  Developed by: Divyansh Kumar 
  Created at: 04-May-2015
  Purpose: To control signup process
*/

window.app
  .controller('SignUpController', ['$scope', '$rootScope', 'Session', '$location', 'Notify', 'Merchant', function ($scope, $rootScope, Session, $location, Notify, Merchant) {
    // To highlight signup link in navigation bar.. 
    $rootScope.selectedNavBar = $rootScope.navBar.signup;
    // To enable registration btn
    $scope.enableRegistrationBtn = true;

    // Error handler for merchant registration
    var registrationErrorHandler = function(response) {
      // Must be enable registration button
      $scope.enableRegistrationBtn = true;
      if (response.statusCode == 422) 
        Notify.error('Form validation fail.', 4000);
      else
        Notify.error(response.data.error, 4000);
    };

    // Success handler for merchant registration
    var registrationSuccessHandler = function(response) {
      // Must be enable registration button
      $scope.enableRegistrationBtn = true;
      Notify.success('Registration Successful.', 4000);
      // Store confirmation email in session (to make accessible for ConfirmationController)
      Session.create('confirmationEmail', response.data.user.email);
      // Redirect to resend confirmation page.
      $location.path('/confirmationRequired');
    };

    // Merchant registration request maker
    $scope.merchantRegistration = function() {
      // To ensure, signup form is valid.
      if (this.signup.$valid) {
        // Must be disable registration button
        $scope.enableRegistrationBtn = false;
        // Preparing input parameters for merchant registration api call
        var postData = {
                          first_name: $scope.first_name,
                          last_name: $scope.last_name,
                          phone_no: $scope.phone_no,
                          email: $scope.email,
                          password: $scope.password,
                          password_confirmation: $scope.password_confirmation,
                          wallet_password: $scope.wallet_password,
                          wallet_password_confirmation: $scope.wallet_password_confirmation,
                          notification_url: $scope.notification_url,
                          tax_id_no: $scope.tax_id_no
                        };
        // Making API call for merchant registration.
        Merchant.registration(postData).then(registrationSuccessHandler, registrationErrorHandler);
      }
      else{
        // Signup form peristine must be false to show validation messsages
        this.signup.$pristine = false;
        // All field must be dirty who are required, to enable error message below field.
        angular.forEach(this.signup.$error.required, function(field) {
          field.$setDirty();
        });
        // All field must be dirty who are required, to enable error message below field.
        setFieldsDirty(this.signup, ['terms', 'password_confirmation', 'wallet_password_confirmation'], true);
        // Must be enable registration button
        $scope.enableRegistrationBtn = true;
      };
    };
    
  }]);
