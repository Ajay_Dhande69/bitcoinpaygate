/*
  Developed by: Divyansh Kumar
  Created at: 06/05/2015
  purpose: To validate uniqueness of input field
*/

window.app.directive('unique', ["$http", function($http) {
  var toId;
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attr, ctrl) { 
      //when the scope changes, check the email.
      scope.$watch(attr.ngModel, function(value) {
        // if there was a previous attempt, stop it.
        if(toId) clearTimeout(toId);

        // start a new attempt with a delay to keep it from
        // getting too "chatty".
        toId = setTimeout(function(){
          $http.post('/api/users/exist', {email: value}).success(function(data) {
              //set the validity of the field
              ctrl.$setValidity('unique', !data);
          });
        }, 200);
      })
    }
  }
}]);