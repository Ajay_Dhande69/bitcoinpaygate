'use strict';

/*
Developer:- Sneha Wangle 
Purpose:- To provide profile page for merchant
Created at: 06/05/2015
*/

window.app.factory('Account', ["$http", '$q', 'AuthService', function ($http, $q, AuthService) {
  var self = {};

  
  // Purpose: To get current rate of BTC
  self.currency_rate = function() {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to register merchant
    $http.get("/api/wallets/currency_rate")
      .success(function(response, statusCode){
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(error, statusCode){
        // returning error response
        deferred.reject({data: error, statusCode: statusCode});
      });
    return deferred.promise;
  };

  // Purpose: To save bank details of merchant wallet from parameters for conversion of BTC to USD
  self.bank_accounts = function(postData) {
    // Must be signed in
    if(!AuthService.loginVerified())
      return false;
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to register merchant
    $http.post("/api/bank_accounts", postData)
      .success(function(response, statusCode){
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(error, statusCode){
        // returning error response
        deferred.reject({data: error, statusCode: statusCode});
      });
    return deferred.promise;
  };

  return self;
}]);