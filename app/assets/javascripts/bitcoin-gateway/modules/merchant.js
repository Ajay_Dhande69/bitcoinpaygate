'use strict';

/*
Developer:- Divyansh Kumar 
Purpose:- To combine actions related to merchant
Created at: 06/05/2015
*/

window.app.factory('Merchant', ["$http", '$q', function ($http, $q) {
  var self = {};

  // To make new merchant registration, it takes hash object of all parameters required to complete registration
  self.registration = function (postData) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to register merchant
    $http.post("/users", postData)
      .success(function(response, statusCode) {
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(response, statusCode) {
        // returning error response
        deferred.reject({data: response, statusCode: statusCode});
      });
    return deferred.promise;
  };

  // To resend email verification mail
  self.resendConfirmation = function (merchantEmail) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to resend email verification mail to merchant email address
    $http.post("/api/users/resend_confirmation", {email: merchantEmail})
      .success(function(response, statusCode) {
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(response, statusCode) {
        // returning error response
        deferred.reject({data: response, statusCode: statusCode});
      });
    return deferred.promise;
  };

  // To mail reset password link
  self.sendResetPasswordLink = function (merchantEmail) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to send reset password link to merchant email address
    $http.post("/users/password", {user: {email: merchantEmail}})
      .success(function(response, statusCode) {
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(response, statusCode) {
        // returning error response
        deferred.reject({data: response, statusCode: statusCode});
      });
    return deferred.promise;
  };

  // To reset password, resetpasswordParams should be {user: {password: <password>, password_confirmation: <password confirmation>}, reset_password_token: <token>}
  self.resetPassword = function (resetPasswordParams) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to reset password for merchant
    $http.put("/users/password", resetPasswordParams)
      .success(function(response, statusCode) {
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(response, statusCode) {
        // returning error response
        deferred.reject({data: response, statusCode: statusCode});
      });
    return deferred.promise;
  };

  return self;
}]);