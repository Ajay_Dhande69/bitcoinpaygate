'use strict';

/*
Developer:- Sneha Wangle 
Purpose:- To provide profile page for merchant
Created at: 06/05/2015
*/

window.app.factory('Profile', ["$http", '$q', 'AuthService', function ($http, $q, AuthService) {
  var self = {};

  // Purpose: To get total amount in btc of merchant wallet from wallet address parameter
  self.total_amount = function () {
    // Must be signed in
    if(!AuthService.loginVerified())
      return false;
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to register merchant
    $http.get("/api/wallets/balance")
      .success(function(response){
        // returning success response
        deferred.resolve(response);
      })
      .error(function(error){
        // returning error response
        deferred.resolve('Unable to get balance, try again');
      });
    return deferred.promise;
  };
  
  var limit = 10;
  // Purpose: To get total amount in btc of merchant wallet from wallet address parameter
  self.transaction_details = function () {
    // Must be signed in
    if(!AuthService.loginVerified())
      return false;
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to register merchant
    $http.get("/api/transactions/" + limit)
      .success(function(response){
        // returning success response
        deferred.resolve(response);
      })
      .error(function(error){
        // returning error response
        deferred.resolve(error);
      });
    return deferred.promise;
  };

  self.details = function() {
    // Must be signed in
    if(!AuthService.loginVerified())
      return false;
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to register merchant
    $http.get("/api/users/details")
      .success(function(response, statusCode){
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(error, statusCode){
        // returning error response
        deferred.resolve({data: error, statusCode: statusCode});
      });
    return deferred.promise;
  };
  
  return self;
}]);