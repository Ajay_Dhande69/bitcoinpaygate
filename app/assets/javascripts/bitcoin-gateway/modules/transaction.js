'use strict';

/*
Developer:- Divyansh Kumar
Purpose:- To provide API calls for transaction functionality
Created at: 15/05/2015
*/

window.app.factory('Transaction', ["$http", '$q', function ($http, $q) {
  var self = {};

  // Purpose: To receive bitcoin to merchant wallet, it requires transaction id
  self.receivePayment = function (transaction_id) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to initiate trecieve transaction process
    $http.put("/api/transactions/"+transaction_id)
      .success(function(response, statusCode){
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(error, statusCode){
        // returning error response
        deferred.resolve({data: error, statusCode: statusCode});
      });
    return deferred.promise;
  };

  // Purpose: To cancel receive transaction process, it requires transaction id
  self.paymentCancel = function (transaction_id) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to cancel trecieve transaction process
    $http.delete("/api/transactions/"+transaction_id)
      .success(function(response, statusCode){
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(error, statusCode){
        // returning error response
        deferred.resolve({data: error, statusCode: statusCode});
      });
    return deferred.promise;
  };

  // Purpose: To cancel receive transaction process, it requires transaction id
  self.QRTransaction = function (transaction_id) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to cancel trecieve transaction process
    $http.get("/api/transactions/"+transaction_id+"/complete")
      .success(function(response, statusCode){
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(error, statusCode){
        // returning error response
        deferred.resolve({data: error, statusCode: statusCode});
      });
    return deferred.promise;
  };

  // Purpose: To cancel receive transaction process, it requires transaction id
  self.GUIDTransaction = function (transaction_id, GUIDDetails) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // Calling API to cancel trecieve transaction process
    $http.post("/api/transactions/"+transaction_id+"/complete", GUIDDetails)
      .success(function(response, statusCode){
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode});
      })
      .error(function(error, statusCode){
        // returning error response
        deferred.resolve({data: error, statusCode: statusCode});
      });
    return deferred.promise;
  };
  
  return self;
}]);