'use strict';

/*
  Developer Name: Divyansh Kumar
  Created at: 08/05/2015
  Purpose: To provide user authentication services
*/

window.app.service('AuthService', ["$http", "Session", "$q", "$rootScope", '$location', function ($http, Session, $q, $rootScope, $location) {

  var self = {};

  // To destory authorization header from browser session, pass true for disable signedout event
  self.destroyAuthHeader = function (disableEventBroadcasting) {
    disableEventBroadcasting = typeof disableEventBroadcasting !== 'undefined' ? disableEventBroadcasting : false;
    //Destory UE and UT from browser session.
    Session.destroy('UE');
    Session.destroy('UT');
    // Broadcast logout event to logout all module
    if(disableEventBroadcasting)
      $rootScope.$broadcast("AuthService:signedOut");
  };

  // To maintain login, UEvalue is user email and UTvalue is user token.
  self.createAuthHeader = function (UEvalue, UTvalue) {
    // Store signed in user email as UE in session (Must be pass as header with each API call)
    Session.create('UE', UEvalue);
    // Store signed in user authentication token as UT in session (Must be pass as header with each API call)
    Session.create('UT', UTvalue);
    // Broadcast logout event to logout all module
    $rootScope.$broadcast("AuthService:signedIn");
  };

  // To get object of UE and UT session values
  self.authHeader = function () {
    return {UE: Session.get('UE'), UT: Session.get('UT')};
  };

  // To verify browser session has UE and UT variable.
  self.loginVerified = function() {
    if (Session.get('UE') != undefined && Session.get('UT') != undefined)
      return true;
    else
      return false;
  };
 
  /*
    To get user login.
    credentials: {email: <username>, password: <password>}
  */
  self.login = function (credentials) {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    $http.post("/users/sign_in", credentials)
      .success(function (response, statusCode) {
        //Store user email and token
        self.createAuthHeader(response.user.email, response.user.authentication_token);
        // returning success response
        deferred.resolve({data: response, statusCode: statusCode})
      })
      .error(function(response, statusCode) {
        // Ensure, UE and UT variable must be destroy.
        self.destroyAuthHeader();
        // returning error response
        deferred.reject({data: response, statusCode: statusCode});
      });

    return deferred.promise;
  };

  self.verify_login = function() {
    // Creating background process synchronizer.
    var deferred = $q.defer();
    // To check UE & UT session variable present or not
    if (self.loginVerified()){
      $http.get("/api/users/verify_token")
        .success(function (response, statusCode) {
          // Broadcast logout event to logout all module
          $rootScope.$broadcast("AuthService:signedIn");
          deferred.resolve({data: response, statusCode: statusCode});
        })
        .error(function(response, statusCode) {
          // Ensure, UE and UT variable must be destroy.
          self.destroyAuthHeader();
          // returning error response
          deferred.reject({data: response, statusCode: statusCode});
        });
    }
    else{
      // Ensure, UE and UT variable must be destroy.
      self.destroyAuthHeader();
      deferred.reject({data: {}, statusCode: 422});
    }

    return deferred.promise;
  };

  self.signOut = function() {
    // $http.delete("/users/sign_out").then(null);
    self.destroyAuthHeader();
    $rootScope.$broadcast("AuthService:signedOut");
  };

  self.mustBeLogin = function() {
    if (self.loginVerified()) {
      return true;
    }
    else{
      $location.path("/");
      return false;
    };
  };

  self.mustBeLogout = function() {
    if (self.loginVerified()) {
      $location.path("/");
      return false;
    }
    else{
      return true;
    };
  };
 
  return self;
}]);