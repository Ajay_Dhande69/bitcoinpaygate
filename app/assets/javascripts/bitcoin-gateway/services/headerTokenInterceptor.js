'use strict';

/*
  Developer Name: Divyansh Kumar
  Created at: 11/May/2015
  Purpose: To set header dynamically.
*/


window.app.factory('HeaderTokenInterceptor', ['$q', 'Session', '$rootScope', '$location', function ($q, Session, $rootScope, $location) {
  return {
    request: function (config) {
      config.headers['X-User-Email'] = Session.get('UE');
      config.headers['X-User-Token'] = Session.get('UT');
      return config || $q.when(config);
    },
    responseError: function(rejection) {
      if(rejection.status == 401){
        //Destory UE and UT from browser session.
        Session.destroy('UE');
        Session.destroy('UT');
        $rootScope.$broadcast("AuthService:signedOut");
        $location.path('/signin');
      }
      else
        console.log(rejection);
      return $q.reject(rejection);
    }
  };
}]);
