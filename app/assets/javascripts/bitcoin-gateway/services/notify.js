'use strict';

/*
  Developer Name: Divyansh Kumar
  Created at: 22/Jan/2015
  Purpose: To provide notification system.
*/

window.app.service('Notify', ['$window', 'notifications', function ($window, notifications) {

  //  To show message with red colored background.
  this.error = function (message, timeout) {
    if (typeof message == 'string') {
      if (typeof(timeout) == 'number' && timeout > 0)
        notifications.showError({message: message, hideDelay: timeout, hide: true});
      else
        notifications.showError({message: message});
    }
  };

  //  To show message with green colored background.
  this.success = function (message, timeout) {
    if (typeof message == 'string') {
      if (typeof(timeout) == 'number' && timeout > 0)
        notifications.showSuccess({message: message, hideDelay: timeout, hide: true});
      else
        notifications.showSuccess({message: message});
    }
  };

  //  To show message with orange colored background.
  this.warning = function (message, timeout) {
    if (typeof message == 'string') {
      if (typeof(timeout) == 'number' && timeout > 0)
        notifications.showWarning({message: message, hideDelay: timeout, hide: true});
      else
        notifications.showWarning({message: message});
    }
  };
  
  return this;
}]);