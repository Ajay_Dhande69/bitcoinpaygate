'use strict';

/*
  Developer Name: Divyansh Kumar
  Created at: 18/Jan/2015
  Modified at: 16/Mar/2015
  Purpose: To provide interface b/w browser session and angular application.
*/

window.app.service('Session', ['$window', function ($window) {

  //  To create new session variable.
  this.create = function (sessionName, data) {
    $window.sessionStorage[sessionName] = JSON.stringify(data);
    return data;
  };

  //  To get value of session variable.
  this.get = function (sessionName) {
    if ($window.sessionStorage[sessionName] == "undefined" || $window.sessionStorage[sessionName] == undefined)
      return undefined;
    return JSON.parse($window.sessionStorage[sessionName]);
  };

  //  To update value of session variable.
  this.put = function (sessionName, data) {
    $window.sessionStorage[sessionName] = JSON.stringify(data);
    return data;
  };

  //  To destory session variable (Actually set to undefined).
  this.destroy = function (sessionName) {
    if ($window.sessionStorage[sessionName] == "undefined" || $window.sessionStorage[sessionName] == undefined)
      return undefined;
    var tmp = JSON.parse($window.sessionStorage[sessionName]);
    $window.sessionStorage[sessionName] = undefined;
    return tmp;
  };
  
  return this;
}]);