class Api::BankAccountsController < ApplicationController

	acts_as_token_authentication_handler_for User, only: [:create]
  
  ##--
  # Created On: 08/06/2015
  # Purpose: To save the account details of merchant
  # GET /api/wallets/balance
  ##++
  def create
    # Initialize object of Bank account with provided parameters 
    details = current_user.bank_accounts.new(bank_accounts_params)
    # render error for invalid limit parameter
    render json: {error: 'Unable to save account details, insufficient balance'}, status: 417 and return if BankAccount.amount(details.btc_amount) == false 
    # return and save the bank account details if parameters values are valid
    if details.valid?
      details.save
      render json: details, status: :created
    else
    	# return error messages if parameters values are invalid
      render json: {errors: details.errors }, status: :unprocessable_entity
    end
  end

  private

    def bank_accounts_params
      params.permit(:account_number, :bank_name, :bank_code, :address, :country, :city, :currency, :btc_amount, :swift_code, :currency_rate)
    end
end
