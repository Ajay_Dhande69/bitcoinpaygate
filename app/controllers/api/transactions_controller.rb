class Api::TransactionsController < ApplicationController
  # To validate token authentication for merchant user
  acts_as_token_authentication_handler_for User, only: [:index]
  # To validate merchant authentication for create action
  before_action :authenticate_merchant!, only: [:create]
  # To validate transaction id
  before_action :validate_transaction_id!, only: [:update, :qr_transaction, :guid_transaction, :destroy]
  # No of retry to convert local currency to btc
  NO_OF_RETRY = BitcoinPaymentGateway::Application.config.blockchain['wallet_create_retry_count']
  # Set default render format
  respond_to :json

	##--
  # Created On: 08/05/2015
  # Purpose: To show the transaction details of merchant wallet
  # GET /api/transactions
  ##
  # Purpose: To show the selected no. of transaction details of merchant wallet
  # GET /api/transactions/:limit
  # params: {limit: <limit>}
  #++
  def index
    # render error for invalid limit parameter
    render json: {error: 'Invalid limit, only digits are allowed'}, status: :unprocessable_entity and return if params[:limit].present? && !can_number?(params[:limit])
    #@transactions = current_user.transactions.order("created_at DESC").last(params[:limit].to_i)
    # To fetch the transaction history of current user with limit
    @transactions = current_user.processed_transactions.order("created_at DESC").last(params[:limit].to_i)
  end

  ##--
  # Created On: 13/05/2015
  # Purpose: To create/initialize transaction
  # POST /api/transactions/
  # params: {api_token: <api_token>, secret_key: <secret_key>, amount: <amount in 
  #          number>, currency: <currency code>, return_url: <http://url>, cancel_url: 
  #          <http://url>}
  ##++
  def create
    retry_count = 0
    begin
      @transaction = current_merchant.initTransaction(params[:amount], params[:currency], params[:return_url], params[:cancel_url])
    rescue StandardError => e
      # Retry 
      if retry_count < NO_OF_RETRY
        retry_count += 1
        retry
      end
      Rails.logger.info "Unable to convert currency(no of try: #{NO_OF_RETRY}): #{e.message}"
      render json: {error: "Unable to convert currency", debug: "#{e.message}"}, status: 417 and return
    end
    render json: {errors: @transaction.errors}, status: :unprocessable_entity and return if @transaction.new_record?
  end

  ##--
  # Created On: 15/05/2015
  # Purpose: To initialize transaction
  # PUT /api/transactions/
  # params: {transaction_id}
  ##++
  def update
    @transaction = current_transaction
    unless current_transaction.receive_btc
      render json: {error: 'Unable to receive bitcoin, try again', cancel_url: @transaction.cancel_url}, status: 417
      return
    end
    # Must change transaction state, setup to initialize
    @transaction.initialize!
  end

  ##--
  # Created On: 18/05/2015
  # Purpose: To process transaction for QR
  # GET /api/transactions/:transaction_id/complete
  # params: {}
  ##++
  def qr_transaction
    @transaction = current_transaction
    # Prepare post data to send via notification url
    post_data = {transaction_id: @transaction.transaction_id, status: @transaction.status, amount: @transaction.amount, currency: @transaction.currency, amount_in_btc: @transaction.amount_in_btc, from_address: @transaction.from_address}
    # Requesting notification url if transaction successed
    RestClient.post(@transaction.user.notification_url, post_data) rescue nil if @transaction.success?
    # Must change transaction state, initialize to pending if transaction has not successed
    @transaction.pending! unless @transaction.success?
  end

  ##--
  # Created On: 18/05/2015
  # Purpose: To process transaction for GUID
  # POST /api/transactions/:transaction_id/complete
  # params: {}
  ##++
  def guid_transaction
    transaction = current_transaction
    errors = validate_guid_credential
    render json: {errors: errors}, status: :unprocessable_entity and return if errors.present?
    response_number = transaction.receive_btc_by_guid(params[:guid], params[:main_password], params[:second_password])
    unless response_number.zero?
      failture_message = case response_number
                          when 2..4
                            "invalid credentials"
                          when 5
                            "unprocessable amount"
                          when 6
                            "insufficient balance"
                          when 7
                            "wallet api disabled"
                          else
                            "unable to process"
                          end
      callback_params = "transaction_id=#{transaction.transaction_id}&status=#{@transaction.status}&message=#{failture_message}&type=guid"
      @redirect_to = prepare_request_url(transaction.cancel_url, callback_params)
    else
      callback_params = "transaction_id=#{transaction.transaction_id}&status=#{@transaction.status}&message=#{failture_message}&type=guid"
      @redirect_to = prepare_request_url(transaction.return_url, callback_params)
    end
  end

  ##--
  # Created On: 18/05/2015
  # Purpose: To cancel transaction for GUID/QR
  # DELETE /api/transactions/:transaction_id
  # params: {}
  ##++
  def destroy
    @transaction = current_transaction
    # Must change transaction state, initialize to cancel if transaction has not successed
    @transaction.cancel! unless @transaction.success?
  end
  
  ##--
  # Created On: 12/05/2015
  # Purpose: To show the calculated price of product in BTC used for payment through QR-code on payment details page
  # POST /api/transactions/tobtc/:currency/:amount
  # params: {currency: <currency>, amount: <amount in number>}        
  #++
  def tobtc
    # To return error message if currency not found or currency length not equal to 3 characters
    render json: {error: 'invalid currency code'}, status: :unprocessable_entity and return unless params[:currency].present? && params[:currency].length == 3 
    # To raise error if pass wrong argument to parameter amount
    render json: {error: 'Invalid amount, only digits are allowed'}, status: :unprocessable_entity and return if params[:amount].present? && !can_number?(params[:amount])
    begin 
      # To fetch the converted price of product
      @price = Transaction.currency_to_btc(params[:currency], params[:amount])
      render json: @price, status: :ok
    # To return error message if error raises by restclient 
    rescue Exceptions::RestClientError => e
      Rails.logger.info "Unable to convert currency: #{e.message}"
      render json: {error: 'Unable to convert currency', debug: "#{e.message}"}, status: 417
      return
    # To return error message when invalid parameters
    rescue Exceptions::TransactionError::ParamsError => e
      render json: {error: "#{e.message}"}, status: 411
    end
  end

  ##--
  # Created On: 12/05/2015
  # Purpose: To show the calculated price of product in BTC for payment through guid(including network fees) on payment details page
  # GET /api/transactions/tobtc_with_fee/:currency/:amount
  # params: {currency: <currency>, amount: <amount in number>}
  #++
  def tobtc_with_fee
    # To return error message if currency not found or currency length not equal to 3 characters
    render json: {error: 'Invalid currency code'}, status: :unprocessable_entity and return unless params[:currency].present? && params[:currency].length == 3 
    # To raise error if pass wrong argument to parameter amount
    render json: {error: 'Invalid amount, only digits are allowed'}, status: :unprocessable_entity and return if params[:amount].present? && !can_number?(params[:amount])
    begin 
      # To fetch the converted price of product
      @price = Transaction.currency_to_btc_with_fee(params[:currency], params[:amount])
      render json: @price, status: :ok
      # To return error message if error raises by restclient 
    rescue Exceptions::RestClientError => e
      Rails.logger.info "Unable to convert currency(with fee): #{e.message}"
      render json: {error: 'Unable to convert currency', debug: "#{e.message}"}, status: 417
    return 
    # To return error message when invalid parameters
    rescue Exceptions::TransactionError::ParamsError => e
      Rails.logger.info "Unable to convert currency(with fee): #{e.message}"
      render json: {error: "#{e.message}"}, status: 411
    end
  end

  ##--
  # Created On: 21/05/2015
  # Purpose: To get payment receive notification from blockchain and also notify to 
  #          merchant application for same.
  # GET /api/transactions/status
  # params: {transaction_id: <transaction id>, value: <satoshi value>, input_address: <BTC sender address>, transaction_hash: <transaction hash>, confirmations: <no of node confirmation>, destination_address: <BTC receiver address>, tid: <transaction object id>}
  #++
  def status
    # seperate tid from transaction_id
    transaction_id, tid = params[:transaction_id].split(",")
    # find transaction by tranaction_id
    @transaction = Transaction.find_by(transaction_id: transaction_id)
    # respond '*ok*' for invalid transaction_id
    render_ok and return unless @transaction.present?
    # respond '*ok*' for invalid tid. To ensure that notification request by blockchain
    render_ok and return unless tid == Digest::MD5.hexdigest("#{@transaction.id}")
    # respond '*ok*' for miss-match destination_address
    render_ok and return unless @transaction.wallet.wallet_address == params[:destination_address]
    # confirmations count must be greater than or equals to 6
    if params[:confirmations].to_i >= BitcoinPaymentGateway::Application.config.blockchain['transaction_confirmations']
      previous_btc_amount = @transaction.amount_in_btc
      # to ensure that @transaction object update successfully.
      if @transaction.update(amount_in_btc: Wallet.satoshi_to_btc(params[:value].to_i), transaction_hash: params[:transaction_hash], from_address: params[:input_address], status: 2) # status 2 is success
        # prepare merchant application request with some usefull parameters
        callback_url = prepare_notification_url(previous_btc_amount)
        # Send notification to merchant application
        response = RestClient.get(callback_url) rescue nil
        # Must check that notification accepted by merchant with '*ok*' in response
        render_ok and return if response && response.body == BitcoinPaymentGateway::Application.config.blockchain['callback_stopper']
      end
    end
    # response to blockchain without no body.
    head :ok
  end

  ##--
  # Created On: 27/05/2015
  # Purpose: To get list of currency code
  # GET /api/transactions/currency_codes
  # params: {}
  ##++
  def currency_codes
    render json: { currency_code: BitcoinPaymentGateway::Application.config.blockchain['currency_code'] }, status: :ok
  end

  private

    def validate_guid_credential
      errors = {}
      errors.merge!({guid: ["can't be blank"]}) unless params[:guid].present?
      errors.merge!({main_password: ["can't be blank"]}) unless params[:main_password].present?
      errors
    end

    def prepare_notification_url(previous_btc_amount)
      notification_url = @transaction.user.notification_url
      callback_params = {transaction_id: @transaction.transaction_id, previous_btc_amount: previous_btc_amount, amount_in_btc: @transaction.amount_in_btc, confirmations: params[:confirmations], type: "qr"}.to_param
      prepare_request_url(notification_url, callback_params)
    end

    def render_ok
      render text: BitcoinPaymentGateway::Application.config.blockchain['callback_stopper'], status: :ok
    end

end

