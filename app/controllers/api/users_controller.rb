class Api::UsersController < ApplicationController
  acts_as_token_authentication_handler_for User, only: [:verify_token, :details]
  before_action :set_merchant, only: [:verify_token, :details]

  # Set default render format
  respond_to :json

  ##--
  # Created at: 05/05/2015
  # Purpose: To check email address already taken or not
  # POST /api/users/exist
  # params: {email}
  ##++
  def exist
    render json: User.where(email: params[:email]).exists?, status: :ok
  end

  ##--
  # Created at: 06/05/2015
  # Purpose: To resend confirmation mail to user email address
  # POST /api/resend_confirmation
  # params: {email}
  ##++
  def resend_confirmation
    user = User.find_by(email: params[:email])
    if user.present?
      if user.unverified?
        user.send_confirmation_instructions rescue render json: {error: 'Unable to send confirmation email, try again'}, status: 411 and return
        render json: {email: user.email}, status: :ok
        return
      else
        render json: {error: 'Email already verified.'}, status: 422
        return
      end
    else
      render json: {error: 'Email doesn\'t exist.'}, status: 404
        return
    end
  end

  ##--
  # Created at: 07/05/2015
  # Purpose: To verify email by confirmation_token for API call
  # POST /api/users/confirmation
  # params: {confirmation_token}
  ##++
  def confirmation
    user = User.confirm_by_token(params[:confirmation_token])
    if user.errors.empty?
      user.active!
      UsersMailer.welcome_mail(user).deliver_later
      render json: true, status: :ok
    else
      render json: {error: 'Invalid confirmation token'}, status: :unprocessable_entity
    end
  end

  ##--
  # Created at: 07/05/2015
  # Purpose: To verify user email & user token, and return user details
  # GET /api/users/verify_user
  ##++
  def verify_token
  end

  ##--
  # Created at: 12/05/2015
  # Purpose: To get account details of signed in  merchant
  # GET /api/users/keys
  ##++
  def details
  end

  private

    def set_merchant
      @merchant = current_user
    end

end
