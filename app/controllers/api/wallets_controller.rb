class Api::WalletsController < ApplicationController

  acts_as_token_authentication_handler_for User, only: [:balance, :index]
  
  # No of retry to fetch current rate of BTC
  NO_OF_RETRY = BitcoinPaymentGateway::Application.config.blockchain['wallet_create_retry_count']

  ##--
  # Created On: 06/05/2015
  # Purpose: To show the total balance(in BTC) of merchant wallet
  # GET /api/wallets/balance
  ##++
  def balance
    begin
      # To fetch wallet balance from third party API 
      balance = current_user.wallet.balance
      render json: balance, status: :ok
    # Hold exception for wallet balance and return error message
    rescue Exceptions::WalletError::WalletBalanceError => e
      render json: {errors: {wallet_address: ["#{e.message}"]}}, status: 411
      return
    # Hold exception for http client and return error message
    rescue Exceptions::RestClientError => e
      render json: {error: "#{e.message}"}, status: 417
      return
    end
  end
  
  ##--
  # Created On: 04/06/15
  # Purpose: To show the wallet details of merchant
  # GET /api/wallets
  ##++
  def index
    # To fetch wallet address from current user 
    render json: current_user.wallet
  end
  
  ##--
  # Created On: 04/06/15
  # Purpose: To show the current rate of BTC
  # GET /api/wallets/currency_rate
  ##++
  def currency_rate
    # Sometimes, blockchain api unable to fetch current rate, retry_count veriable work for it.
    retry_count = 0 
    begin
      # To fetch current rate of BTC from third party API 
      currency_rate = Wallet.current_rate
      render json: currency_rate, status: :ok
    # Hold exception for http client and return error message
    rescue StandardError => e
      # Retry 
      if retry_count < NO_OF_RETRY
        retry_count += 1
        retry
      end
      # After retry failure return error message
      Rails.logger.info "Unable to fetch current BTC rate(no of try: #{NO_OF_RETRY}): #{e.message}"
      render json: {error: "Unable to fetch current BTC rate", debug: "#{e.message}"}, status: 417 and return
    end  
  end
end
