class ApplicationController < ActionController::Base
  include ApplicationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  protect_from_forgery with: :null_session

  ##--
  ## Created On: 02/05/2015
  ## Purpose: To render angular framework
  #++
  def index
  end

  ##--
  ## Created On: 05/05/2015
  ## Purpose: To verify input parameters
  #++
  def verify_params(render_enable, *validates)
    errors = {}
    validates.each do |validate|
      errors.merge({"#{validate}": ["should be present"]}) if !params[validate].present?
    end
    render json: {errors: errors}, status: :unprocessable_entity if errors.present? && render_enable
    errors
  end

  ##--
  ## Created On: 08/05/2015
  ## Purpose: To verify the value of parameters
  #++
  def can_number?(value)
    !(value =~ /\A[0-9]+\Z/).nil?
  end

  ##--
  ## Created On: 05/05/2015
  ## Purpose: To must authenticate merchant
  #++
  def authenticate_merchant!
    @current_merchant = User.find_by_keys(params[:api_token], params[:secret_key])
    render json: {error: 'Access denied'}, status: 401 and return unless @current_merchant.present?
    render json: {error: 'merchant account out of service'}, status: 406 unless @current_merchant.active?
  end

  ##--
  ## Created On: 05/05/2015
  ## Purpose: To get current authenticated merchant
  #++
  def current_merchant
    @current_merchant ||= User.find_by_keys(params[:api_token], params[:secret_key])
  end

  ##--
  ## Created On: 05/05/2015
  ## Purpose: To validate transaction id that a transaction can only access one time.
  #++
  def validate_transaction_id!
    @transaction = Transaction.find_by(transaction_id: params[:transaction_id])
    render json: {error: 'invalid transaction id', redirect_to: ENV["URL"]}, status: 406 and return if @transaction.nil?
    render json: {error: 'merchant account out of service', redirect_to: ENV["URL"]}, status: 406 and return unless @transaction.user.active?
    if !validate_transaction_state(@transaction)
      @transaction.invalidState! unless params[:action] == "update"
      render json: {error: 'invalid transaction id', redirect_to: "#{@transaction.cancel_url}?status=#{@transaction.status}"}, status: 406
      return
    end
  end

  ##--
  ## Created On: 05/05/2015
  ## Purpose: To get current authenticated transaction
  #++
  def current_transaction
    @transaction
  end

  private

    def validate_transaction_state(transaction)
      return true if ["qr_transaction", "destroy"].include?(params[:action]) && !transaction.setup?
      action_map = {"update" => "setup?", "guid_transaction" => "initialize?"}
      return true if transaction.send(action_map[params[:action]]) rescue nil
      false
    end

end
