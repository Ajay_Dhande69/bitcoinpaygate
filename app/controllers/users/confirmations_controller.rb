class Users::ConfirmationsController < Devise::ConfirmationsController
  # GET /resource/confirmation/new
  # def new
  #   super
  # end

  # POST /resource/confirmation
  # def create
  #   super
  # end

  ##++
  # Created at: 07/05/2015
  # Purpose: To verifiy email address and activate merchant account to login
  # GET /users/confirmation?confirmation_token=abcdef
  ##--
  def show
    # finding user by token or initialize new record with error message for confirmation_token
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    # check error message present or not, if error message present, unable to find user by token
    if resource.errors.empty?
      # Activating merchant account
      resource.active!
      UsersMailer.welcome_mail(resource).deliver_later
      # Redirecting to confirmed route of angular
      redirect_to '/email/confirmed'
    else
      # Redirecting to unconfirmed route of angular
      redirect_to '/email/unconfirmed'
    end
  end

  # protected
  # The path used after resending confirmation instructions.
  # def after_resending_confirmation_instructions_path_for(resource_name)
  #   super(resource_name)
  # end

  # The path used after confirmation.
  # def after_confirmation_path_for(resource_name, resource)
  #   super(resource_name, resource)
  # end
end
