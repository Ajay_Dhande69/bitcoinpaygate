class Users::RegistrationsController < Devise::RegistrationsController

  # No of retry to create wallet
  NO_OF_RETRY = BitcoinPaymentGateway::Application.config.blockchain['wallet_create_retry_count']

  # Set default render format
  respond_to :json
  # before_filter :configure_sign_up_params, only: [:create]
  # before_filter :configure_account_update_params, only: [:update]

  ##++
  # Created at: 07/05/2015
  # Purpose: To register new merchant
  # POST /users
  # Params: { first_name, last_name, phone_no, email, password,password_confirmation, wallet_password, wallet_password_confirmation, notification_url, tax_id_no }
  # 
  ##--
  def create
    # Initialize object of User with provided paramsters 
    @merchant = User.new(merchant_registration_params)
    # return error messages if parameters values are invalid
    render json: {errors: @merchant.errors }, status: :unprocessable_entity and return if @merchant.invalid?
    # To validate wallet password
    return unless validate_wallet_password
    # Sometimes, blockchain api unable to create wallet, retry_count veriable work for it.
    retry_count = 0
    # Wallet creation is not reliable. So, logic must be in transaction (because in case of failture, we can rollback ).
    ActiveRecord::Base.transaction do
      begin
        # Try to create a wallet
        wallet = Wallet.create_wallet(params[:wallet_password])
        # Save registration details (if unable to save, will raise Exception)
        @merchant.save!
        # Save wallet details (if unable to save, will raise Exception)
        @merchant.create_wallet!(wallet_address: wallet["address"], wallet_password: params[:wallet_password], guid: wallet["guid"])
      # Hold password length exception for wallet and return error message
      rescue Exceptions::WalletError::PasswordLengthError => e
        render json: {errors: {wallet_password: ["#{e.message}"]}}, status: :unprocessable_entity
        # Must be rollback, to prevent garbage entries.
        raise ActiveRecord::Rollback and return
      # Hold wallet creation exception and retry
      rescue StandardError => e
        # Retry 
        if retry_count < NO_OF_RETRY
          retry_count += 1
          retry
        else
          Rails.logger.info "Wallet creation failed(no of try: #{NO_OF_RETRY}): #{e.message}"
          render json: {error: error_message(e), debug: "#{e.message}"}, status: 417
          raise ActiveRecord::Rollback and return
        end
      end
    end
  end

  # DELETE /resource
  # def destroy
  #   super
  # end

  private

    def merchant_registration_params
      params.permit(:first_name, :last_name, :email, :phone_no, :password, :password_confirmation, :notification_url, :tax_id_no)
    end

    def error_message(error_class)
      if error_class.class == TypeError
        'wallet password must be string'
      else
        'Unable to create wallet'
      end
    end

    def validate_wallet_password
      errors = {}
      errors.merge!({wallet_password: ['can\'t be blank']}) if params[:wallet_password].blank?
      errors.merge!({wallet_password_confirmation: ['doesn\'t match wallet password']}) if params[:wallet_password_confirmation] != params[:wallet_password]
      render json: {errors: errors}, status: :unprocessable_entity and return false if errors.present?
      true
    end
end
