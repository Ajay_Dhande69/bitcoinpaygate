class Users::SessionsController < Devise::SessionsController
  acts_as_token_authentication_handler_for User, only: [:destroy]
  respond_to :json

  ##++
  # Created at: 09/05/2015
  # Purpose: To get authentication token
  # POST /users/sign_in
  # Params: { user: {email: <demo@example.com>, password: <password>} }
  # 
  ##--
  def create
    resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
    render json: {error: 'Account deactivated'}, status: 403 and return if resource.deactive?
    resource.reset_auth_token!
    @resource = resource
  end

  def failure
    warden.custom_failure!
    render json: {error: "Login Failed"}, status: 401
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end
end
