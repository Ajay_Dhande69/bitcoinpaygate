module ApplicationHelper

  def prepare_request_url(url, params_string)
    no_query = URI.parse(url).query.nil?
    url += '/' if no_query && url.scan('/').count
    if URI.parse(url).query.nil?
      "#{url}?#{params_string}"
    else
      "#{url}&#{params_string}"
    end
  end
end
