class UsersMailer < Devise::Mailer
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default template_path: 'users/mailer' # to make sure that you mailer uses the devise views

  def reset_password_instructions(record, token, opts={})
    super
  end

  def confirmation_instructions(record, token, opts={})
    super
  end

  def welcome_mail(record)
    @merchant = record
    @wallet = record.wallet
    mail(to: @merchant.email, subject: 'Welcome to Bitcoin payment gateway')
  end
end