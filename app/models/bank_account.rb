##++
# Table name: bank_accounts
# columns: [“account_number”, “bank_code”, “country”, “city", “btc_amount”, “bank_name”, “swift_code”, “address”, “currency”, “currency_rate”, "created_at",
#           "updated_at"]
#
# Methods:
#         amount: To compare btc_amount with merchant wallet balance
#
##-- 

class BankAccount < ActiveRecord::Base
  belongs_to :user

  #validate
  validates_numericality_of :btc_amount, presence: true, greater_than: 0

  validates :account_number, 
             presence: true,
             format: {
              with: /\A[0-9\-]*\Z/,
              message: "should be digits only"
            },
            length: {
              maximum: 100,
              too_long: "should be at most %{count} digits"
            }

  validates :bank_name,
            presence: true,
            format: {
              with: /\A[a-z][a-z ]*\Z/i,
              message: "should be letters and whitespace only"
            },
            length: {
              maximum: 255,
              too_long: "should be at most %{count} characters"
            }

  validates :bank_code, 
            presence: true,
            format: {
              with: /\A[a-zA-Z0-9]*\Z/,
              message: "should be alphanumeric"
            },
            length: {
              maximum: 50,
              too_long: "should be at most %{count} characters"
            }

  validates :address, 
            presence: true,
            length: {
              maximum: 255,
              too_long: "should be at most %{count} characters"
            }
            
  validates :country, 
            presence: true,
            format: {
              with: /\A[a-zA-Z0-9 ]*\Z/,
              message: "should be letters, digits and whitespaces only"
            },
            length: {
              maximum: 255,
              too_long: "should be at most %{count} characters"
            }

  validates :city, 
            presence: true,
            format: {
              with: /\A[a-zA-Z0-9 ]*\Z/,
              message: "should be letters, digits and whitespaces only"
            },
            length: {
              maximum: 100,
              too_long: "should be at most %{count} characters"
            }

  validates :swift_code, 
            presence: true,
            format: {
              with: /\A[A-Z0-9]*\Z/,
              message: "should be alphanumeric"
            },
            length: {
              maximum: 50,
              too_long: "should be at most %{count} characters"
            }

  validates_with CustomValidator::CurrencyValidate          

  validates_numericality_of :currency_rate, presence: true 
  # for any number validation use this regex:(/\A[\d]+[\.]{0,1}[\d]*\Z/)
   

  # To compare btc_amount with merchant wallet balance
  def self.amount(btc_amount)
    # get current user
    user = User.verified.last
    # merchant wallet balance
    balance = user.wallet.balance
    # compare balance to btc_amount
    balance >= btc_amount 
  end

end
