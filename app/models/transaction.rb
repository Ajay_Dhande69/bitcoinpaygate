##++
# Table name: transactions
# columns: ["id", "user_id", "amount_in_btc", "amount", "currency", "transaction_id",
#           "from_address", "to_address", "status", "transaction_hash", "message", 
#           "notice", "return_url", "cancel_url", "created_at", "updated_at"]
# Callbacks:
#           set_keys: To set transaction_id for each new record
#
#
# Methods:
#         currency_to_btc: To convert the price of product from local currency to 
#                          BTC
#
#         currency_to_btc_with_fee: To convert the price of product from local 
#                                   currency to BTC for payment through guid
#
##--

class Transaction < ActiveRecord::Base
  belongs_to :user, required: true
  has_one :wallet, through: :user

  enum status: [:setup, :initialize, :success, :pending, :fail, :cancel, :invalidState]

  validates_numericality_of :amount

  validates_numericality_of :amount_in_btc, greater_than_or_equal_to: BitcoinPaymentGateway::Application.config.blockchain['minimum_btc_amount'], allow_nil: true

  validates_with CustomValidator::CurrencyValidate

  validates :return_url,
            presence: true,
            format: {with: /\A(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\Z/},
            length: {
              maximum: 255,
              too_short: "should be at least %{count} characters",
              too_long: "should be at most %{count} characters"
            }

  validates :cancel_url,
            presence: true,
            format: {with: /\A(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\Z/},
            length: {
              maximum: 255,
              too_short: "should be at least %{count} characters",
              too_long: "should be at most %{count} characters"
            }

  after_initialize :set_transaction_id, if: :new_record?

  ##--
  # Created at: 13/05/2015
  # Purpose: To set transaction_id to new record
  ##++
  def set_transaction_id
    loop do
      token = SecureRandom.base64(32).tr('+/=', 'Qrt')
      break self.transaction_id = token unless Transaction.exists?(transaction_id: token)
    end
  end

  ##--
  ## Created On: 12/05/2015
  ## Purpose: To convert the price of product from local currency to BTC
  ##++
  def self.currency_to_btc(currency_code, amount)
    # Use YAML to load blockchain.yml file for fetching api code and blockchain api for create wallet
    request_url = BitcoinPaymentGateway::Application.config.blockchain["api"]["exchange_rate"] % {currency_code: currency_code, amount: amount}
    # If in response failure occured using third party API or http client it generate error and return nil into the response 
    response =  begin
                  RestClient.get(request_url) 
                rescue Exception => e
                  raise Exceptions::RestClientError, e.message
                  nil
                end
    # If response is success(code == 200) it return price in BTC
    if response && response.code == 200
      # it gives the the converted value in BTC
      response.body.to_f
    else
      # To raise the error if error occured in executing blockchain api 
      raise Exceptions::TransactionError::ParamsError, response.body
    end
  end

  ##--
  ## Created On: 12/05/2015
  ## Purpose: To convert the price of product from local currency to BTC with network 
  ##          fee for payment through guid
  ##++
  def self.currency_to_btc_with_fee(currency_code, amount)
    # To calculate the amount with network fees
    Transaction.currency_to_btc(currency_code, amount) + Wallet.satoshi_to_btc(BitcoinPaymentGateway::Application.config.blockchain["transaction_fee"])
  end

  ##--
  ## Created On: 15/05/2015
  ## Purpose: To update BTC amount by real time BTC value
  ##++
  def update_btc_amount
    real_time_btc = Transaction.currency_to_btc(self.currency, self.amount) rescue nil
    unless real_time_btc.nil?
      update(amount_in_btc: real_time_btc)
    else
      false
    end
  end

  ##--
  ## Created On: 15/05/2015
  ## Purpose: To call blockchian api to receive BTC
  ##++
  def receive_btc
    return false unless update_btc_amount
    # Prepare params, transaction object id must be pass to blockchain after Md5 digest
    set_return_url = URI.encode("#{Rails.application.routes.url_helpers.status_api_transactions_url}?transaction_id=#{transaction_id},#{Digest::MD5.hexdigest(id.to_s)}")
    # Prepare api call url
    request_url = BitcoinPaymentGateway::Application.config.blockchain["api"]["generate_receiving_address"] % {receiver_address: wallet.wallet_address, return_url: set_return_url}
    begin
      response =  RestClient.get(request_url)
      if response && response.code == 200
        # parse response to json
        response = JSON.parse(response.body)
        # update to_address in table
        update(to_address: response['input_address'])
      else
        false
      end
    rescue Exception => e
      Rails.logger.info "Unable to receive_btc: #{e.message}"
      Rails.logger.debug "Unable to receive_btc: #{self.inspect}"
      false
    end
  end

  def self.transaction_by_guid(receiver_address, amount_in_btc, guid, guid_pass, guid_pass2)
    # convert btc to satoshi and add transaction fee
    amount = (btc_to_satoshi(amount_in_btc) + Wallet.satoshi_to_btc(BitcoinPaymentGateway::Application.config.blockchain["transaction_fee"])).ceil
    # Prepare params
    request_url = BitcoinPaymentGateway::Application.config.blockchain["api"]["make_payment_by_guid"] % {guid: guid || '', main_password: guid_pass || '', second_password: guid_pass2 || '', address: receiver_address, amount: amount}
    # If in response failure occured using third party API or http client it generate error and return nil into the response 
    response =  begin
                  RestClient.get(request_url, {"Content-Type": "application/x-www-form-urlencoded"})
                rescue Exception => e
                  raise Exceptions::RestClientError, e.message
                  nil
                end
    # If response is success(code == 200)
    if response && response.code == 200
      # it gives the the converted value in BTC
      JSON.parse(response.body)
    else
      # To raise the error if error occured in executing blockchain api 
      raise Exceptions::TransactionError::ParamsError, response.body
    end
  end

  def receive_btc_by_guid(guid, main_password, second_password)
    response = self.class.transaction_by_guid(to_address, amount_in_btc, guid, main_password, second_password) rescue nil
    if response && response['error'].nil?
      update_flag = update(message: response['message'], notice: response['notice'], transaction_hash: response['tx_hash'], status: 3)
      # Must set transaction status to fail if system unable to update record
      self.fail! unless update_flag
      return (update_flag ? 0 : 1) # 0 for successful transaction and 1 for transaction complete but unable to save record.
    else
      # Must set transaction status to fail
      self.fail!
      if response && response['error'].present?
        Rails.logger.debug "guid transaction api error: transaction_id:#{transaction_id}"
        Rails.logger.info "guid transaction api error: #{response['error']}"
        return 2 if response['error'] =~ /Format/i # invalid GUID
        return 3 if response['error'] =~ /corrupted/i # invalid password
        return 4 if response['error'] =~ /Decrypting/i # password blank
        return 5 if response['error'] =~ /satoshi fee/i # amount less than fee
        return 6 if response['error'] =~ /Insufficient/i # insufficient balance
        return 7 if response['error'] =~ /disabled/i # insufficient balance
      end
      return -1
    end
  end

  def self.btc_to_satoshi(btc)
    btc * 100000000
  end

end
