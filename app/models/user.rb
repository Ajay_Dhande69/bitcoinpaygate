##++
# Table name: users
# columns: ["id", "email", "encrypted_password", "reset_password_token", 
#           "reset_password_sent_at", "confirmation_token", "confirmed_at", 
#           "confirmation_sent_at", "first_name", "last_name", "secret_key", 
#           "api_token", "authentication_token", "notification_url", "phone_no", 
#           "status", "created_at", "updated_at", "tax_id_no", "password_salt"]
# Callbacks:
#           set_keys: To set api_token and secret_key for each new record
#
#
# Methods:
#         full_name: To return concat of first_name and last-name
#         verified?: To check email verified or not
#         verified: To return all verified email users
#         unverfifed: To return all unverified email users
#         active: To reutrn all active users
#         deactive: To return all deactive users
#         initTransaction: Take six parameters and returns transaction id to initialize 
#                          new transaction
#
#
#
##--

class User < ActiveRecord::Base
  has_one :wallet, dependent: :destroy
  has_many :transactions, dependent: :destroy
  has_many :bank_accounts, dependent: :destroy 

  enum status: [ :unverified, :active, :deactive ]

  # Validations
  validates :first_name,
            presence: true,
            format: {
              with: /\A[a-z][a-z ]*\Z/i,
              message: "should be letters and whitespace only"
            }

  validates :last_name,
            format: {
              with: /\A[a-z][a-z ]*\Z/i,
              message: "should be letters and whitespace only"
            },
            allow_nil: true,
            allow_blank: true

  validates :phone_no,
            presence: true,
            length: {
              minimum: 7,
              maximum: 15,
              too_short: "should be at least %{count} words",
              too_long: "should be at most %{count} words"
            },
            format: {with: /\A\d{7,15}\Z/}

  validates :notification_url,
            presence: true,
            format: {with: /\A(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\Z/},
            length: {
              maximum: 255,
              too_long: "should be at most %{count} characters"
            }

  validates :tax_id_no, presence: true

  validates :email, 
            presence: true,
            format: {with: /\A([A-za-z0-9._-]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}

  validates :password, presence: true, format: {with: /\A[\s]*[\S]+[\S\s]*\Z/, message: "Please avoid whitespace"}, if: :new_record?

  acts_as_token_authenticatable

  devise :database_authenticatable, :registerable,
         :recoverable, :validatable, :confirmable, :encryptable

  after_initialize :set_keys, if: :new_record?

  scope :verified, -> { where.not(status: 0) }

  scope :unverified, -> { where(status: 0) }

  scope :active, -> { where(status: 1) }

  scope :deactive, -> { where(status: 2) }

  ##--
  # Created at: 07/05/2015
  # Purpose: To set api_token and secret_key to new record
  ##++
  def set_keys
    loop do
      token = SecureRandom.hex(32)
      break self.secret_key = token unless User.exists?(secret_key: token)
    end
    loop do
      token = SecureRandom.base64(32).tr('+/=', 'Qrt')
      break self.api_token = token unless User.exists?(api_token: token)
    end
  end

  ##--
  # Created at: 07/05/2015
  # Purpose: Returns full name, first_name must be set
  ##++
  def full_name
    "#{first_name} #{last_name || ''}" if first_name.present?
  end

  ##--
  # Created at: 07/05/2015
  # Purpose: To check email verified or not
  ##++
  def verified?
    !unverified?
  end

  ##--
  # Created at: 13/05/2015
  # Purpose: To find user by keys(api_token and secret_key)
  ##++
  def self.find_by_keys(api_token, secret_key)
    where(api_token: api_token, secret_key: secret_key).limit(1).first
  end

  ##--
  # Created at: 13/05/2015
  # Purpose: Take six parameters and returns transaction id to initialize new 
  #          transaction
  ##++
  def initTransaction(amount, currency_code, return_url, cancel_url)
    # Prepare columns and values pair to initialize transaction 
    transaction_parameters = {amount: amount, currency: currency_code, return_url: return_url, cancel_url: cancel_url}
    # Initialize transaction object with prepared parameters
    transaction = transactions.new(transaction_parameters)
    # Check transaction object validation
    if transaction.valid?
      transaction.amount_in_btc = Transaction.currency_to_btc(currency_code, amount)
      transaction.save
    end
    transaction
    # Note: must be check that return object has saved in table or check for errors on returned object
  end

  ##--
  # Created at: 13/05/2015
  # Purpose: To find all transactions of user excepted initialized transaction
  ##++
  def processed_transactions
    transactions.where.not(status: 0)
  end

  def reset_auth_token!
    self.authentication_token = nil
    self.ensure_authentication_token
    save
  end
end
