##++
# Table name: wallets
# columns: ["id", "user_id", "wallet_address", "guid", "wallet_password", "created_at",
#           "updated_at"]
# Callbacks:
#           method_name: description
#
#
# Methods:
#         create_wallet: To make blockchain api call and return new wallet details, not 
#                        saved to table
#
#         wallet_balance: To make blockchain api call and return wallet balance in 
#                         satoshi
#
#         satoshi_to_btc: To calculate the amount in BTC from satoshi
#                        (1 BTC = 0.00000001 Satoshi)
#
#         current_rate: To make blockchain api call and return current rate of BTC 
##--

class Wallet < ActiveRecord::Base
  belongs_to :user, required: true

  ##--
  ## Created On: 04/05/2015
  ## Purpose: To create wallet for merchant
  #++
  def self.create_wallet(wallet_password)
    # To raise type error if provide another datatype instead of string
    raise TypeError, "wallet password must be string" unless wallet_password.is_a?(String)
    # To raise error if password length is less than 10
    raise Exceptions::WalletError::PasswordLengthError if wallet_password.length < 10
	  post_parameters = {password: wallet_password, api_code: BitcoinPaymentGateway::Application.config.blockchain["api_code"]}
    # If in response failure occured using third party API or http client it generate error and return nil into the response 
    response =  begin
                  RestClient.post(BitcoinPaymentGateway::Application.config.blockchain["api"]["create_wallet"], post_parameters)
                rescue Exception => e
                  raise Exceptions::RestClientError, e.message
                  nil
                end
    if response && response.code == 200
      result_hash = JSON.parse(response.body)
    else
      # To raise the error if error occured in executing blockchain api 
      raise Exceptions::WalletError::UnableToCreateError, response.body
	  end
  end

  ##--
  ## Created On: 06/05/2015
  ## Purpose: To fetch wallet balance for merchant
  #++
  def self.wallet_balance(wallet_address)
    request_url = BitcoinPaymentGateway::Application.config.blockchain["api"]["address_balance"] % {wallet_address: wallet_address, confirmation: "#{BitcoinPaymentGateway::Application.config.blockchain['balance_confirmation_count']}"}
    # If in response failure occured using third party API or http client it generate error and return nil into the response 
    response =  begin
                  RestClient.get(request_url) #1Kvf4cHhxSecSsoSetRttsCXBatmRrpQDh
                rescue Exception => e
                  raise Exceptions::RestClientError, e.message
                  nil
                end
    # If response is success(code == 200) it return amount in BTC
    if response && response.code == 200
      # it give the value in Satoshi
      result_hash = response.body
      # To calculate the amount in BTC from satoshi(1 BTC = 0.00000001 Satoshi)
      amount_btc = Wallet.satoshi_to_btc(result_hash.to_f)
      #amount_in_btc = result_hash.to_f * 0.00000001
    else
      # To raise the error if error occured in executing blockchain api 
      raise Exceptions::WalletError::WalletBalanceError, response.body
    end
  end

  def balance
    self.class.wallet_balance(wallet_address)
  end
  
  ##--
  ## Created On: 06/05/2015
  ## Purpose: To calculate the amount in BTC from satoshi(1 BTC = 0.00000001 Satoshi)
  #++
  def self.satoshi_to_btc(value)
    amount_in_btc = value * 0.00000001
  end

  ##--
  ## Created On: 04/06/2015
  ## Purpose: To call blockchian api and return current rate of BTC.
  ##++
  def self.current_rate
    # Prepare api call url
    request_url = BitcoinPaymentGateway::Application.config.blockchain["api"]["current_rate"]
    response =  begin
                  RestClient.get(request_url) 
                rescue Exception => e
                  raise Exceptions::RestClientError, e.message
                  nil
                end
    # If response is success(code == 200) it return current value of BTC
    if response && response.code == 200
      # it give the current amount of BTC for all currency codes
      JSON.parse(response.body)
    else
      # To raise the error if error occured in executing blockchain api 
      raise Exceptions::WalletError::CurrentRateError, response.body
    end
  end
end



