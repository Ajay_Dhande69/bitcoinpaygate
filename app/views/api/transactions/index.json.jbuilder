json.transactions do |json|
  json.array! @transactions do |transaction|
    json.(transaction, :amount_in_btc, :amount, :currency, :transaction_id, :from_address, :status, :transaction_hash, :created_at)
  end
end
