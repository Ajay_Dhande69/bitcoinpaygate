json.user do |json|
  json.(@merchant, :first_name, :last_name, :email, :phone_no, :notification_url, :tax_id_no)
end

json.sign_in_url "/users/sign_in"