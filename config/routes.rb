Rails.application.routes.draw do
  
  namespace :api do
    resources :bank_accounts, only: [:create], defaults: { format: 'json' } 
      
    resources :users, only: [], defaults: { format: 'json' } do
      collection do
        post 'exist'
        post 'resend_confirmation'
        post 'confirmation'
        get 'verify_token'
        get 'details'
      end
    end 
    resources :wallets, only: [:index], defaults: { format: 'json' } do 
      collection do
        get 'balance'
        get 'currency_rate'
      end
    end
    resources :transactions, param: 'transaction_id', only: [:index, :create, :update, :destroy], defaults: { format: 'json' } do
      collection do
        get 'tobtc/:currency/:amount', to: 'transactions#tobtc'
        get 'tobtc_with_fee/:currency/:amount', to: 'transactions#tobtc_with_fee'
        get 'status'
        get 'currency_codes'
        get '/:limit', to: 'transactions#index'
      end
      get 'complete', action: :qr_transaction, on: :member
      post 'complete', action: :guid_transaction, on: :member
    end 
  end

  devise_for :users, controllers: {
    registrations: "users/registrations",
    confirmations: "users/confirmations",
    sessions: "users/sessions",
    passwords: "users/passwords"
  }, defaults: { format: 'json' }

  root 'application#index'
  
  get '*path' => 'application#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
