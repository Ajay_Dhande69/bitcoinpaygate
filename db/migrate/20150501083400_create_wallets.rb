class CreateWallets < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :wallet_address
      t.string :guid
      t.string :wallet_password

      t.timestamps null: false
    end
  end
end
