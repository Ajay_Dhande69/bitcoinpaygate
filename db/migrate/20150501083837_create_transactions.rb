class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.float :amount_in_btc
      t.float :amount
      t.string :currency, limit: 25
      t.string :transaction_id
      t.string :from_address
      t.string :to_address
      t.integer :status, limit: 1, default: 0
      t.string :transaction_hash
      t.string :message
      t.string :notice
      t.string :return_url
      t.string :cancel_url

      t.timestamps null: false
    end

    add_index :transactions, :transaction_id, unique: true
  end
end
