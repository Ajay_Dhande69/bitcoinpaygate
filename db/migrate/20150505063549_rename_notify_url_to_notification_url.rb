class RenameNotifyUrlToNotificationUrl < ActiveRecord::Migration
  def change
    rename_column :users, :notify_url, :notification_url
  end
end
