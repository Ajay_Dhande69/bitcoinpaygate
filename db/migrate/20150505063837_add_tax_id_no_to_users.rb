class AddTaxIdNoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :tax_id_no, :string
  end
end
