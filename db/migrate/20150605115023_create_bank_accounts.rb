class CreateBankAccounts < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :account_number, limit: 100
      t.string :bank_name
      t.string :bank_code, limit: 50
      t.string :address
      t.string :country, limit: 100
      t.string :city, limit: 100
      t.string :currency, limit: 25
      t.float :btc_amount
      t.string :swift_code, limit: 50
      t.float :currency_rate

      t.timestamps null: false
    end
  end
end
