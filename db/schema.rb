# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150605115023) do

  create_table "bank_accounts", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.string   "account_number", limit: 100
    t.string   "bank_name",      limit: 255
    t.string   "bank_code",      limit: 50
    t.string   "address",        limit: 255
    t.string   "country",        limit: 100
    t.string   "city",           limit: 100
    t.string   "currency",       limit: 25
    t.float    "btc_amount",     limit: 24
    t.string   "swift_code",     limit: 50
    t.float    "currency_rate",  limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "bank_accounts", ["user_id"], name: "index_bank_accounts_on_user_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.integer  "user_id",          limit: 4
    t.float    "amount_in_btc",    limit: 24
    t.float    "amount",           limit: 24
    t.string   "currency",         limit: 25
    t.string   "transaction_id",   limit: 255
    t.string   "from_address",     limit: 255
    t.string   "to_address",       limit: 255
    t.integer  "status",           limit: 1,   default: 0
    t.string   "transaction_hash", limit: 255
    t.string   "message",          limit: 255
    t.string   "notice",           limit: 255
    t.string   "return_url",       limit: 255
    t.string   "cancel_url",       limit: 255
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "transactions", ["transaction_id"], name: "index_transactions_on_transaction_id", unique: true, using: :btree
  add_index "transactions", ["user_id"], name: "index_transactions_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "secret_key",             limit: 255
    t.string   "api_token",              limit: 255
    t.string   "authentication_token",   limit: 255,              null: false
    t.string   "notification_url",       limit: 255
    t.string   "phone_no",               limit: 20
    t.integer  "status",                 limit: 1,   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tax_id_no",              limit: 255
    t.string   "password_salt",          limit: 255
  end

  add_index "users", ["api_token"], name: "index_users_on_api_token", unique: true, using: :btree
  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["secret_key"], name: "index_users_on_secret_key", unique: true, using: :btree

  create_table "wallets", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.string   "wallet_address",  limit: 255
    t.string   "guid",            limit: 255
    t.string   "wallet_password", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "wallets", ["user_id"], name: "index_wallets_on_user_id", using: :btree

  add_foreign_key "bank_accounts", "users"
  add_foreign_key "transactions", "users"
  add_foreign_key "wallets", "users"
end
