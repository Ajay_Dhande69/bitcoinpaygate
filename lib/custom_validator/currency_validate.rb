class CustomValidator::CurrencyValidate < ActiveModel::Validator
  def validate(record)
    unless presence_of_currency(record)
      record.errors.add :currency, 'can\'t be blank'
    else
      unless inclusion_of_currency(record)
        record.errors.add :currency, "#{record.currency} is invalid"
      end
    end
  end

  private
    def presence_of_currency(record)
      record.currency.present?
    end

    def inclusion_of_currency(record)
      BitcoinPaymentGateway::Application.config.blockchain['currency_code'].include?(record.currency)
    end
end