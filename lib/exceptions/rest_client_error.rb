class Exceptions::RestClientError < StandardError
  def initialize(msg=nil)
    super(msg)
  end
end