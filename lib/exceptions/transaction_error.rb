module Exceptions::TransactionError
  # To provide error message in case failure using blockchain API
  class ParamsError < StandardError
    def initialize(msg=nil)
      super(msg)
    end
  end
end