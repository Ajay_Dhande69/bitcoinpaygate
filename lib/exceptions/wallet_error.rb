module Exceptions::WalletError
  # To provide error message if wallet password length is less than 10
  class PasswordLengthError < StandardError
    def message
      "Password must be at least 10 characters"
    end
  end
  
  # To provide error message in case failure using blockchain API for wallet creation
  class UnableToCreateError < StandardError
    def initialize(msg=nil)
      super(msg)
    end
  end

  # To provide error message in case failure using blockchain API for fetching balance of wallet
  class WalletBalanceError < StandardError
    def initialize(msg=nil)
      super(msg)
    end
  end

  # To provide error message in case failure using blockchain API for fetching current rate from blockchain API
  class CurrentRateError < StandardError
    def initialize(msg=nil)
      super(msg)
    end
  end
end