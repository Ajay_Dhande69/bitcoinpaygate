require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Divyansh Kumar
# Created on:- 08/05/2015

def login(driver, email, password)
  driver.find_element(:css, "input[name='email']").clear
  driver.find_element(:css, "input[name='password']").clear
  driver.find_element(:css, "input[name='email']").send_keys(email)
  driver.find_element(:css, "input[name='password']").send_keys(password)
  driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[4]/div/button').click
  sleep 8
  expect(driver.current_url).to match(/profile/)
end

describe "To check authentication key details page functionality" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :chrome
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://localhost:3000"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

    @driver.get(@base_url+"/signin")
  end

  after(:all) do
    # Exit browser
    @driver.quit
  end

  it "1. Api token and secret key should match to database, show/hide toggle btn should show and hide secret key." do
    email = 'divyanshkumar@yopmail.com'
    password = '7894561230'
    login(@driver, email, password)
    @driver.find_element(:xpath, '//*[@id="myTab"]/li[2]/a').click
    user = User.find_by(email: email)
    expect(@driver.find_element(:css, "input[ng-model='apiToken']")['value']).to eq(user.api_token)
    expect(@driver.find_element(:css, "input[ng-model='secretKey']")['type']).to eq('password')
    @driver.find_element(:xpath, '//*[@id="tab2"]/div/div/form/div/div[3]/div/span/button').click
    expect(@driver.find_element(:css, "input[ng-model='secretKey']")['type']).to eq('text')
    expect(@driver.find_element(:css, "input[ng-model='secretKey']")['value']).to eq(user.secret_key)
  end

end