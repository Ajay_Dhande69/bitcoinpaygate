require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Sneha Wangle
# Created on:- 09/06/2015

def login(driver, email, password)
  driver.find_element(:css, "input[name='email']").clear
  driver.find_element(:css, "input[name='password']").clear
  driver.find_element(:css, "input[name='email']").send_keys(email)
  driver.find_element(:css, "input[name='password']").send_keys(password)
  driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[4]/div/button').click
  sleep 8
  expect(driver.current_url).to match(/profile/)
end

describe "To check bank account page functionality" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :chrome
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://bitcoinpaygate.herokuapp.com:3000/"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

    @driver.get(@base_url+"/signin")
  end

  after(:all) do
    # Exit browser
    @driver.quit
  end
  
  it "Required" do
    email = 'sneha.wangle@cryptextechnologies.com'
    password = '7894561230'
    login(@driver, email, password)
    @driver.find_element(:xpath, '//*[@id="myTab"]/li[3]/a').click
    user = User.find_by(email: email)
    expect(@driver.current_url).to match(/convert/)
  end

  
end