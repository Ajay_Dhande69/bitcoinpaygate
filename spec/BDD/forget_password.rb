require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Divyansh Kumar
# Created on:- 11/05/2015

describe "To check merchant forget password functionality" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :chrome
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://localhost:3000"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

    @driver.get(@base_url+"/forgotPassword")
  end

  after(:all) do
    # Exit browser
    @driver.quit
  end
  
  it "1. Required field must be filled to login." do
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[2]/div/button').click
    sleep 1
    expect(@driver.current_url).to match(/forgotPassword/)
  end

  it "2. should be raise error message on empty" do
    required_fields = ['email']
    required_fields.each do |field|
      expect(@driver.find_element(:css, "input[name='#{field}'] + div").text).to match(/Enter/)
    end
  end

  it "3. System should raise error if email address is invalid" do
    invalid_email = ['abc', 'sdfsdf.com ']
    email_field = @driver.find_element(:css, "input[name='email']")
    invalid_email.each do |invalid|
      email_field.clear
      email_field.send_keys(invalid)
      expect(@driver.find_element(:css, "input[name='email'] + div").text).to match(/Invalid/)
    end
    email_field.clear
    email_field.send_keys('hygfd4547548@yopmail.com')
    sleep 2
    expect(@driver.find_elements(:css, "input[name='email'] + div").first.text).to eql("")
  end

  it "3. System should raise error if email address is no exist" do
    email = 'zyzyzyzyzyzyyz@gmail.com'
    @driver.find_element(:css, "input[name='email']").clear
    @driver.find_element(:css, "input[name='email']").send_keys(email)
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[2]/div/button').click
    sleep 1
    expect(@driver.current_url).to match(/forgotPassword/)
  end
  
  it "3. Try to reset password for valid email" do
    exist_email = User.last.email
    @driver.find_element(:css, "input[name='email']").clear
    @driver.find_element(:css, "input[name='email']").send_keys(exist_email)
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[2]/div/button').click
    sleep 10
    expect(@driver.current_url).to_not match(/forgotPassword/)
  end

  it "4. To check reset password link is working" do
    # checked manually by divyansh
  end

  it "5. To check password reset successfully after submitting reset password form" do
    # checked manually by divyansh
  end

end