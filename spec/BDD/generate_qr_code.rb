require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Sneha Wangle
# Created on:- 13/05/2015
# Purpose:- To generate QR code
def login(driver, email, password)
  driver.find_element(:css, "input[name='email']").clear
  driver.find_element(:css, "input[name='password']").clear
  driver.find_element(:css, "input[name='email']").send_keys(email)
  driver.find_element(:css, "input[name='password']").send_keys(password)
  driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[4]/div/button').click
  sleep 8
  expect(driver.current_url).to match(/payment/)
end

describe "To show QR code on payment details page" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :firefox
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://localhost:3000"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

  end

  after(:all) do
    # Exit browser
    @driver.quit
  end

  it "1. To show generated QR code" do
    
  end

end
