require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Sneha Wangle
# Created on:- 19/05/2015
# Purpose:- To show home page links

describe "To check home page links" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :firefox
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://bitcoinpaygate.herokuapp.com/"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

    @driver.get(@base_url+"home")
  end

  after(:all) do
    # Exit browser
    @driver.quit
  end
  
  it "1. To show the about us page" do
    @driver.find_element(:xpath, '/html/body/div[1]/div/div[2]/ul/li[2]/a').click
    expect(@driver.current_url).to match(/about/) 
  end

  it "2. To show the pricing page" do
    @driver.find_element(:xpath, '/html/body/div[1]/div/div[2]/ul/li[3]/a').click
    expect(@driver.current_url).to match(/pricing/) 
  end

  it "3. To show the services page" do
    @driver.find_element(:xpath, '/html/body/div[1]/div/div[2]/ul/li[4]/a').click
    expect(@driver.current_url).to match(/services/) 
  end

  it "4. To show the Why payment gateway API page" do
    @driver.find_element(:xpath, '/html/body/div[1]/div/div[2]/ul/li[5]/a').click
    expect(@driver.current_url).to match(/why/) 
  end

  it "5. To show the Contact Us page" do
    @driver.find_element(:xpath, '/html/body/div[1]/div/div[2]/ul/li[6]/a').click
    expect(@driver.current_url).to match(/contact/) 
  end
end