require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Divyansh Kumar
# Created on:- 15/05/2015

describe "To check receive payment page functionality" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :chrome
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://localhost:3000"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

    @transaction = Transaction.last
    @transaction.setup!
    @driver.get(@base_url+"/receivePayment/#{@transaction.transaction_id}")
    sleep 10
  end

  after(:all) do
    # Exit browser
    @driver.quit
  end

  it "1. Receiver wallet address field should pre-filled and disabled" do
    # Click on 'Bitcoin Payment Through QR Code' tab
    @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[1]/a').click
    # select receiver wallet address field
    receiver_address_field = @driver.find_element(:xpath, '//*[@id="tab1"]/div/div/div[1]/div[1]/div[1]/input')
    # Check for, field should be pre-filled
    expect(receiver_address_field['value']).to_not eq('')
    # Check for, field value should be exact 34 characters.
    expect(receiver_address_field['value'].length).to eq(34)
    # Check for, field must be disabled 
    expect(receiver_address_field['disabled']).to eq("true")
  end

  it "2. Actual Amount field should pre-filled and disabled" do
    # Click on 'Bitcoin Payment Through QR Code' tab
    @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[1]/a').click
    # select actual amount field
    actual_amount_field = @driver.find_element(:xpath, '//*[@id="tab1"]/div/div/div[1]/div[1]/div[3]/input')
    # Check for, field should be pre-filled
    expect(actual_amount_field['value']).to_not eq('')
    # Check for, field must contain valid amount.
    expect(actual_amount_field['value']).to match(/\A[0-9]*\.?[0-9]+ [A-Z]+\Z/)
    # Check for, field must be disabled 
    expect(actual_amount_field['disabled']).to eq("true")
  end

  it "3. BTC field should pre-filled and disabled" do
    # Click on 'Bitcoin Payment Through QR Code' tab
    @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[1]/a').click
    # select BTC field
    btc_field = @driver.find_element(:xpath, '//*[@id="tab1"]/div/div/div[1]/div[1]/div[4]/input')
    # Check for, field should be pre-filled
    expect(btc_field['value']).to_not eq('')
    # Check for, field must contain valid amount.
    expect(btc_field['value']).to match(/\A[0-9]*\.?[0-9]+\Z/)
    # Check for, field must be disabled 
    expect(btc_field['disabled']).to eq("true")
  end

  it "4. Receiver wallet address field should pre-filled and disabled ('Bitcoin Payment Through GUID' tab)" do
    # Click on 'Bitcoin Payment Through GUID' tab
    @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[2]/a').click
    # select receiver wallet address field
    receiver_address_field = @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[1]/input')
    # Check for, field should be pre-filled
    expect(receiver_address_field['value']).to_not eq('')
    # Check for, field value should be exact 34 characters.
    expect(receiver_address_field['value'].length).to eq(34)
    # Check for, field must be disabled 
    expect(receiver_address_field['disabled']).to eq("true")
  end

  it "5. Actual Amount field should pre-filled and disabled ('Bitcoin Payment Through GUID' tab)" do
    # Click on 'Bitcoin Payment Through GUID' tab
    @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[2]/a').click
    # select actual amount field
    actual_amount_field = @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[3]/div/div[1]/input')
    # Check for, field should be pre-filled
    expect(actual_amount_field['value']).to_not eq('')
    # Check for, field must contain valid amount.
    expect(actual_amount_field['value']).to match(/\A[0-9]*\.?[0-9]+ [A-Z]+\Z/)
    # Check for, field must be disabled 
    expect(actual_amount_field['disabled']).to eq("true")
  end

  it "6. BTC field should pre-filled and disabled ('Bitcoin Payment Through GUID' tab)" do
    # Click on 'Bitcoin Payment Through GUID' tab
    @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[2]/a').click
    # select BTC field
    btc_field = @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[3]/div/div[3]/input')
    # Check for, field should be pre-filled
    expect(btc_field['value']).to_not eq('')
    # Check for, field must contain valid amount.
    expect(btc_field['value']).to match(/\A[0-9]*\.?[0-9]+\Z/)
    # Check for, field must be disabled 
    expect(btc_field['disabled']).to eq("true")
  end

  it "7. By click on cancel btn, user should redirect to cancel url" do
    # Click on 'Bitcoin Payment Through QR Code' tab
    @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[1]/a').click
    # Click on 'cancel' btn
    @driver.find_element(:xpath, '//*[@id="tab1"]/div/div/div[3]/div[2]/button').click
    sleep 4
    # Check for, browser redirect to cancel_url
    expect(@driver.current_url).to match(/#{@transaction.cancel_url}/)
  end

  # it "8. By click on cancel btn, user should redirect to cancel url ('Bitcoin Payment Through GUID' tab)" do
  #   # Click on 'Bitcoin Payment Through GUID' tab
  #   @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[2]/a').click
  #   # Click on 'cancel' btn
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[3]/div[2]/button').click
  #   sleep 4
  #   # Check for, browser redirect to cancel_url
  #   expect(@driver.current_url).to match(/#{@transaction.cancel_url}/)
  # end

  # it "9. By click on confirm btn, user should redirect to return url ('Bitcoin Payment Through QR Code' tab)" do
  #   # Click on 'Bitcoin Payment Through QR Code' tab
  #   @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[1]/a').click
  #   # Click on 'confirm' btn
  #   @driver.find_element(:xpath, '//*[@id="tab1"]/div/div/div[3]/div[1]/button').click
  #   sleep 4
  #   # Check for, browser redirect to return_url
  #   expect(@driver.current_url).to match(/#{@transaction.return_url}/)
  # end

  # it "10. Pay by wrong GUID credential" do
  #   # Replace XXXXXXXXXXXXXXX by wrong GUID
  #   guid = "1a28071b-12d8-4a6b-aa1e-b1040866488c"
  #   # Replace XXXXXXXXXXXXXXX by wrong GUID password
  #   guid_password = "qwerty@12125"
  #   # Click on 'Bitcoin Payment Through GUID' tab
  #   @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[2]/a').click
  #   # Clear field
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[2]/input').clear
  #   # Enter GUID
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[2]/input').send_keys(guid)
  #   # Clear field
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[4]/div/div[1]/input').clear
  #   # Enter GUID password
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[4]/div/div[1]/input').send_keys(guid_password)
  #   # Click on 'confirm' btn
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[3]/div[1]/button').click
  #   sleep 5
  #   # Check for, browser redirect to cancel_url
  #   expect(@driver.current_url).to match(/#{@transaction.cancel_url}/)
  # end

  # it "11. Pay by correct GUID credential with insufficient balance" do
  #   # Replace XXXXXXXXXXXXXXX by correct GUID
  #   guid = "1a28071b-12d8-4a6b-aa1e-b1040866488c"
  #   # Replace XXXXXXXXXXXXXXX by correct GUID password
  #   guid_password = "qwerty@1234"
  #   # Click on 'Bitcoin Payment Through GUID' tab
  #   @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[2]/a').click
  #   # Clear field
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[2]/input').clear
  #   # Enter GUID
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[2]/input').send_keys(guid)
  #   # Clear field
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[4]/div/div[1]/input').clear
  #   # Enter GUID password
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[4]/div/div[1]/input').send_keys(guid_password)
  #   # Click on 'confirm' btn
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[3]/div[1]/button').click
  #   sleep 5
  #   # Check for, browser redirect to return_url with insufficient balance message
  #   expect(@driver.current_url).to match(/status=fail&message=insufficient%20balance/)
  # end

  # it "12. Pay by correct GUID credential with sufficient balance" do
  #   # Replace XXXXXXXXXXXXXXX by correct GUID
  #   guid = "1a28071b-12d8-4a6b-aa1e-b1040866488c"
  #   # Replace XXXXXXXXXXXXXXX by correct GUID password
  #   guid_password = "qwerty@1234"
  #   # Click on 'Bitcoin Payment Through GUID' tab
  #   @driver.find_element(:xpath, '/html/body/div[2]/div/div/article/div/div/div[1]/ul/li[2]/a').click
  #   # Clear field
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[2]/input').clear
  #   # Enter GUID
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[2]/input').send_keys(guid)
  #   # Clear field
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[4]/div/div[1]/input').clear
  #   # Enter GUID password
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[1]/div[4]/div/div[1]/input').send_keys(guid_password)
  #   # Click on 'confirm' btn
  #   @driver.find_element(:xpath, '//*[@id="tab2"]/div/form/div[3]/div[1]/button').click
  #   sleep 5
  #   # Check for, browser redirect to return_url with success status
  #   expect(@driver.current_url).to match(/status=success/)
  # end

end