require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Divyansh Kumar
# Created on:- 08/05/2015

describe "To check merchant signin functionality" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :chrome
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://localhost:3000"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

    @driver.get(@base_url+"/signin")
  end

  after(:all) do
    # Exit browser
    @driver.quit
  end
  
  it "1. Required field must be filled to login." do
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[4]/div/button').click
    expect(@driver.current_url).to match(/signin/)
  end

  it "2. should be raise error message on empty" do
    required_fields = ['email', 'password']
    required_fields.each do |field|
      expect(@driver.find_element(:css, "input[name='#{field}'] + div").text).to match(/Enter/)
    end
  end
  
  it "3. Try to login with incorrect credential." do
    email = 'sdfsdfsdf@edecfd.com'
    password = '789456'
    @driver.find_element(:css, "input[name='email']").send_keys(email)
    @driver.find_element(:css, "input[name='password']").send_keys(password)
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[4]/div/button').click
    sleep 5
    expect(@driver.current_url).to match(/signin/)
  end

  it "4. Try to login with correct credential but account deactivated." do
    email = 'sdfsdfsdf@edecfd.com'
    password = '7894561230'
    @driver.find_element(:css, "input[name='email']").clear
    @driver.find_element(:css, "input[name='password']").clear
    @driver.find_element(:css, "input[name='email']").send_keys(email)
    @driver.find_element(:css, "input[name='password']").send_keys(password)
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[4]/div/button').click
    sleep 10
    expect(@driver.current_url).to match(/signin/)
  end

  it "5. Try to login with correct credential." do
    email = 'divyanshkumar@yopmail.com'
    password = '7894561230'
    @driver.find_element(:css, "input[name='email']").clear
    @driver.find_element(:css, "input[name='password']").clear
    @driver.find_element(:css, "input[name='email']").send_keys(email)
    @driver.find_element(:css, "input[name='password']").send_keys(password)
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[4]/div/button').click
    sleep 10
    expect(@driver.current_url).to match(/profile/)
  end

end