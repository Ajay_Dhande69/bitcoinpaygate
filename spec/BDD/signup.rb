require "selenium-webdriver"
require "rspec"
require "byebug"

# Created by:- Divyansh Kumar
# Created on:- 04/05/2015

describe "To check merchant registration process" do

  before(:all) do
    # Open firefox browser
    @driver = Selenium::WebDriver.for :chrome
    # Maximize the window of browser
    @driver.manage.window.maximize
    # defined base URL
    @base_url = "http://localhost:3000/"
    # Wait for page to load
    @driver.manage.timeouts.implicit_wait = 10

    @driver.get(@base_url+"/signup")
  end

  after(:all) do
    # Exit browser
    @driver.quit
  end
  
  it "1. Required field must be filled to complete registration process." do
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[10]/button').click
    expect(@driver.current_url).to match(/signup/)
  end

  it "2. System Should be raise error for required field (in case of empty)." do
    required_fields = ['first_name', 'phone_no', 'email', 'password', 'wallet_password', 'notification_url', 'tax_id_no']
    required_fields.each do |field|
      expect(@driver.find_element(:css, "input[name='#{field}'] + div").text).to match(/Enter/)
    end
    expect(@driver.find_element(:css, "label.checkbox + div").text).to match(/accept terms/)
  end
  
  it "3. System should raise error if first name has not alphabetical characters only." do
    invalid_first_names = ['abcdef4', '34fdsf ', 'fsd$#', '235234234', ' asdsa34d']
    first_name_field = @driver.find_element(:css, "input[name='first_name']")
    invalid_first_names.each do |invalid|
      first_name_field.clear
      first_name_field.send_keys(invalid)
      expect(@driver.find_element(:css, "input[name='first_name'] + div").text).to match(/Invalid/)
    end
    str = (0...256).map { (65 + rand(26)).chr }.join
    first_name_field.clear
    first_name_field.send_keys(str)
    expect(@driver.find_element(:css, "input[name='first_name'] + div").text).to match(/at most/)
    first_name_field.clear
    first_name_field.send_keys('abcdef')
    expect(@driver.find_elements(:css, "input[name='first_name'] + div").first.text).to eql('')
  end

  it "4. System should raise error if last name has not alphabetical characters only." do
    invalid_last_name = ['abcdef4', '34fdsf ', 'fsd$#', '235234234', ' asd34sad']
    last_name_field = @driver.find_element(:css, "input[name='last_name']")
    invalid_last_name.each do |invalid|
      last_name_field.clear
      last_name_field.send_keys(invalid)
      expect(@driver.find_element(:css, "input[name='last_name'] + div").text).to match(/Invalid/)
    end
    str = (0...256).map { (65 + rand(26)).chr }.join
    last_name_field.clear
    last_name_field.send_keys(str)
    expect(@driver.find_element(:css, "input[name='last_name'] + div").text).to match(/at most/)
    last_name_field.clear
    last_name_field.send_keys('abcdef')
    expect(@driver.find_elements(:css, "input[name='first_name'] + div").first.text).to eql('')
  end

  it "5. System should raise error if phone no has not numerical characters only or less than 7 characters" do
    invalid_phone_no = ['abcdef4df', '34fdsf gdf', 'fsd$#fdgdfg', ' asdsadfdgfd', ['00000000']]
    phone_no_field = @driver.find_element(:css, "input[name='phone_no']")
    invalid_phone_no.each do |invalid|
      phone_no_field.clear
      phone_no_field.send_keys(invalid)
      expect(@driver.find_element(:css, "input[name='phone_no'] + div").text).to match(/Invalid/)
    end
    phone_no_field.clear
    phone_no_field.send_keys('12345')
    expect(@driver.find_element(:css, "input[name='phone_no'] + div").text).to match(/Invalid/)
    phone_no_field.clear
    phone_no_field.send_keys('123456789012345678901234567890123456')
    expect(@driver.find_element(:css, "input[name='phone_no'] + div").text).to match(/Invalid/)
    phone_no_field.clear
    phone_no_field.send_keys('12345678')
    expect(@driver.find_elements(:css, "input[name='phone_no'] + div").first.text).to eql('')
  end

  it "6. System should raise error if email address is invalid" do
    invalid_email = ['abc', 'sdfsdf.com ']
    email_field = @driver.find_element(:css, "input[name='email']")
    invalid_email.each do |invalid|
      email_field.clear
      email_field.send_keys(invalid)
      expect(@driver.find_element(:css, "input[name='email'] + div").text).to match(/Invalid/)
    end
    exist_email = User.last.email
    email_field.clear
    email_field.send_keys(exist_email)
    sleep 2
    expect(@driver.find_element(:css, "input[name='email'] + div").text).to match(/already/)
    email_field.clear
    email_field.send_keys('hygfd4547548@yopmail.com')
    sleep 2
    expect(@driver.find_elements(:css, "input[name='email'] + div").first.text).to eql("")
  end

  it "7. System should raise error if password length is less than 10 and greater than 128." do
    password_field = @driver.find_element(:css, "input[name='password']")
    password_field.clear
    password_field.send_keys('123457')
    expect(@driver.find_element(:css, "input[name='password'] + div").text).to match(/at least/)
    str = (0...129).map { (65 + rand(26)).chr }.join
    password_field.clear
    password_field.send_keys(str)
    expect(@driver.find_element(:css, "input[name='password'] + div").text).to match(/at most/)
    password_field.clear
    password_field.send_keys('1234567890')
    expect(@driver.find_elements(:css, "input[name='password'] + div").first.text).to eql('')
  end

  it "8. System should raise error if password and confirm password are not equal." do
    password_field = @driver.find_element(:css, "input[name='password']")
    password_field.clear
    password_field.send_keys('1234567890')
    password_confirmation_field = @driver.find_element(:css, "input[name='password_confirmation']")
    password_confirmation_field.clear
    password_confirmation_field.send_keys('1234567')
    expect(@driver.find_element(:css, "input[name='password_confirmation'] + div").text).to match(/match/)
    password_confirmation_field.clear
    password_confirmation_field.send_keys('1234567890')
    expect(@driver.find_elements(:css, "input[name='password_confirmation'] + div").first.text).to eql('')
  end

  it "9. System should raise error if password for wallet length is less than 10 and greater than 128" do
    wallet_password_field = @driver.find_element(:css, "input[name='wallet_password']")
    wallet_password_field.clear
    wallet_password_field.send_keys('1234578')
    expect(@driver.find_element(:css, "input[name='wallet_password'] + div").text).to match(/at least/)
    str = (0...129).map { (65 + rand(26)).chr }.join
    wallet_password_field.clear
    wallet_password_field.send_keys(str)
    expect(@driver.find_element(:css, "input[name='wallet_password'] + div").text).to match(/at most/)
    wallet_password_field.clear
    wallet_password_field.send_keys('1234567890')
    expect(@driver.find_elements(:css, "input[name='wallet_password'] + div").first.text).to eql('')
  end

  it "10. System should raise error if password for wallet and confirm password for wallet are not equal" do
    wallet_password_field = @driver.find_element(:css, "input[name='wallet_password']")
    wallet_password_field.clear
    wallet_password_field.send_keys('1234567890')
    wallet_password_confirmation_field = @driver.find_element(:css, "input[name='wallet_password_confirmation']")
    wallet_password_confirmation_field.clear
    wallet_password_confirmation_field.send_keys('1234567')
    expect(@driver.find_element(:css, "input[name='wallet_password_confirmation'] + div").text).to match(/match/)
    wallet_password_confirmation_field.clear
    wallet_password_confirmation_field.send_keys('1234567890')
    expect(@driver.find_elements(:css, "input[name='wallet_password_confirmation'] + div").first.text).to eql('')
  end

  it "11. System should raise error if notification url is invalid url." do
    notification_url_field = @driver.find_element(:css, "input[name='notification_url']")
    notification_url_field.clear
    notification_url_field.send_keys('sdfsdfsdfsdfs')
    expect(@driver.find_element(:css, "input[name='notification_url'] + div").text).to match(/Invalid/)
    notification_url_field.clear
    notification_url_field.send_keys('http://sdfsdfsdfsdfs.com')
    expect(@driver.find_elements(:css, "input[name='notification_url'] + div").first.text).to eql('')
  end

  it "12. Merchant should be register and redirect to resend confirmation page if there is no validation error." do
    str = (0...18).map { (65 + rand(26)).chr }.join
    form_data = [['first_name', 'divyansh'], ['last_name', 'kumar'], ['phone_no', '8623012098'], ['email', "#{str}@example.com"], ['password', '7894561230'], ['password_confirmation', '7894561230'], ['wallet_password', '7894561230'], ['wallet_password_confirmation', '7894561230'], ['notification_url', 'http://sdfsdfsdfsdfs.com'], ['tax_id_no', 'TIN784544']]
    form_data.each do |data|
      element = @driver.find_element(:css, "input[name='#{data[0]}']")
      element.clear
      element.send_keys(data[1])
    end
    @driver.find_element(:css, "input[name='terms']").click
    @driver.find_element(:xpath , '/html/body/div[2]/div/div/article/div/div/div/form/div[10]/button').click
    sleep 20
    expect(@driver.current_url).to match(/confirmationRequired/)
  end

end