require "rails_helper"
require 'byebug'
require './spec/custom_helpers/authenticate.rb'
require './spec/custom_helpers/colorize.rb'

##--
  # Created By: Sneha Wangle
  # Created On: 05/06/15
  # Purpose: To save the merchant bank details
  ##++
RSpec.describe Api::BankAccountsController, :type => :controller do
  
  params_data = {
    account_number: 7894565654254-5,
    bank_name: "oversease bank",
    bank_code: "AA7894561278", 
    address: "234 hillway road",
    country: "america",
    city: "alaska", 
    currency: "USD", 
    btc_amount: 22.23, 
    swift_code: "FGDSFJKHY322344", 
    currency_rate: 324.3
  }

  describe "POST create" do
    it "1. Should save the bank details" do
      user = User.verified.last
      # Request exist action with parameters      
      post :create, valid_auth_parameters.merge(params_data), format: 'json'
      details = user.bank_accounts.new(params_data)
      if BankAccount.amount(details.btc_amount) == false
        # verify status code
        expect(response.status).to eq(417)
      else
        details = details.save
        #verify details 
        expect(details.present?).to eq(true)
        # verify status code
        expect(response.status).to eq(201)
        # assertion test step
        assert_response :created
        # verify id
        expect(JSON.parse(response.body)["id"]).to eq(BankAccount.last.id)
      end
    end


    it "2. Should not save the bank details with invalid parameters" do
      data = {
        account_number: "78945gfdg33fdgfdg",
        bank_name: "dg12233fhdzghgd",
        bank_code: "fdghdfffffffffg", 
        address: "fdgf22dgf@wqew",
        country: "fdg22fdzg",
        city: "22fdgf", 
        currency: "2w", 
        btc_amount: "djhdsfgd", 
        swift_code: "qqqqqqqqqqq", 
        currency_rate: "gdsaf"
      }
      # Request exist action with parameters 
      post :create, valid_auth_parameters.merge(data), format: 'json'
      # verify status code
      expect(response.status).to eq(422) 
      # assertion test step
      assert_response :unprocessable_entity
      # verfy error
      expect(JSON.parse(response.body)["errors"].class).to eq(Hash) 
    end

    it "3. Should not save the bank details with invalid authentication" do
      # Request exist action with parameters
      post :create, invalid_auth_parameters.merge(params_data), format: 'json'
      # verify status code
      expect(response.status).to eq(401)
      # assertion test step
      assert_response 401
    end
  end

end

