require "rails_helper"
require 'byebug'
require './spec/custom_helpers/authenticate.rb'
require './spec/custom_helpers/colorize.rb'

RSpec.describe Api::TransactionsController, :type => :controller do
  render_views

  describe "GET index" do
    it "list of transactions with 10 limit" do
      # Request exist action with email parameter
      get :index, valid_auth_parameters.merge({limit: 10})
      transactions = User.find_by(email: valid_auth_parameters[:user_email]).processed_transactions.order("created_at DESC").last(10)
      # Check response body
      expect(assigns(:transactions)).to eq(transactions)
    end

    it "list of transactions with 20 limit" do
      # Request exist action with email parameter
      get :index, valid_auth_parameters.merge({limit: 20})
      transactions = User.find_by(email: valid_auth_parameters[:user_email]).processed_transactions.order("created_at DESC").last(20)
      # Check response body
      expect(assigns(:transactions)).to eq(transactions)
    end

    it "should not return list of transactions with invalid auth token" do
      # Request exist action with email parameter
      get :index, invalid_auth_parameters.merge({limit: 20})
      # Check response body
      expect(response.status).to eq(401)
    end
  end
  

# Developer: Sneha Wangle
  describe "GET tobtc" do
    it "should convert local currency to btc" do
      # Request exist action with parameter
      get :tobtc, {currency: "USD", amount: 112}
      # To fetch the converted price of product
      price = Transaction.currency_to_btc("USD", 112)
      # Check response status
      expect(response.status).to eq(200)
    end

    it "should not convert local currency to btc with invalid currency_code" do
      # Request exist action with parameter
      get :tobtc, {currency: "INR", amount: 112}
      # Check response status
      expect(response.status).to eq(417)
    end
    
    it "should not convert local currency to btc with invalid amount" do
      # Request exist action with parameter
      get :tobtc, {currency: "USD", amount: "ssyht11"}
      # Check response status
      expect(response.status).to eq(422)
    end
  end

  describe "POST create" do

    @merchant = User.verified.last
    init_transaction_test_data = [
      # Invalid api_token
      {
        api_token: 'shkdj876s7f6sd78f687sdf6sd7f67sdf876ds',
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com/cancel?username=sdecfd',
        invalid: 'api_token'
      },
      # Invalid secret_key
      {
        api_token: @merchant.api_token,
        secret_key: 'jh3jh34h3hjg4g34hg5j3h4g3h4gjh34gjh4gjh34hj435jh43j5h34',
        amount: 22,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com/cancel?username=sdecfd',
        invalid: 'secret_key'
      },
      # Invalid amount_in_btc (BTC value less than 0.0005)
      {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 0.008,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com/cancel?username=sdecfd',
        invalid: 'amount_in_btc'
      },
      # Invalid currency
      {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USDD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com/cancel?username=sdecfd',
        invalid: 'currency'
      },
      # Invalid cancel_url
      {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USD',
        cancel_url: 'htp://example.com',
        return_url: 'http://example.com/cancel?username=sdecfd',
        invalid: 'cancel_url'
      },
      # Invalid return_url
      {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'htp://example.com/cancel?username=sdecfd',
        invalid: 'return_url'
      },
      {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com'
      }
    ]
    init_transaction_test_data.each do |data|
      it "try to initiate transaction with #{data[:invalid] ? 'invalid' : 'valid'} #{data[:invalid] || 'data'}" do
        post :create, data.merge({format: 'json'})
        if ['api_token', 'secret_key'].include?(data[:invalid])
          expect(response.status).to eq(401)
        else
          if data[:invalid]
            expect(response.status).to eq(422)
            expect(JSON.parse(response.body)["errors"][data[:invalid]].class).to eq(Array)
          else
            expect(response.status).to eq(200)
            expect(JSON.parse(response.body)["transaction"]["amount"]).to eq(data[:amount])
          end
        end
      end
    end
  end

  describe "PUT update" do
    it "receive payment landing page with invalid transaction_id" do
      put :update, transaction_id: 'sdfkjsd7rwe777e7er7er77878', format: 'json'
      expect(response.status).to eq(406)
    end

    it "receive payment landing page with valid transaction_id" do
      @merchant = User.verified.last
      data = {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com'
      }
      post :create, data.merge({format: 'json'})
      # puts green("\nPlease wait for 30 sec to update BTC exchange rate")
      # (120).downto(1) do |n|
      #   printf(green("\r%3d seccond(s) left"), n)
      #   sleep(1)
      # end
      # puts "\n"
      transaction = JSON.parse(response.body)['transaction']
      put :update, transaction_id: transaction['transaction_id'], format: 'json'
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)['transaction']['amount']).to eq(transaction['amount'])
      # expect(JSON.parse(response.body)['transaction']['amount_in_btc']).to_not eq(transaction['amount_in_btc'])
    end
  end

  describe "GET qr_transaction" do
    it "confirm transaction for QR code" do
      @merchant = User.verified.last
      data = {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com'
      }
      post :create, data.merge({format: 'json'})
      transaction = JSON.parse(response.body)['transaction']
      put :update, transaction_id: transaction['transaction_id'], format: 'json'
      expect(response.status).to eq(200)
      get :qr_transaction, transaction_id: transaction['transaction_id'], format: 'json'
      expect(JSON.parse(response.body)['redirect_to']).to eq("#{transaction['return_url']}/?transaction_id=#{transaction['transaction_id']}&status=pending")
    end
  end

  # Real BTC required
  # describe "POST guid_transaction" do
  # end

  describe "DELETE destroy" do
    it "cancel transaction by customer" do
      @merchant = User.verified.last
      data = {
        api_token: @merchant.api_token,
        secret_key: @merchant.secret_key,
        amount: 22,
        currency: 'USD',
        cancel_url: 'http://example.com',
        return_url: 'http://example.com'
      }
      post :create, data.merge({format: 'json'})
      transaction = JSON.parse(response.body)['transaction']
      put :update, transaction_id: transaction['transaction_id'], format: 'json'
      expect(response.status).to eq(200)
      delete :destroy, transaction_id: transaction['transaction_id'], format: 'json'
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)['redirect_to']).to eq("#{transaction['return_url']}/?transaction_id=#{transaction['transaction_id']}&status=cancel")
    end
  end

  describe "GET tobtc" do
    it "convert amount to btc" do
      get :tobtc, currency: "USD", amount: "22"
      expect(response.status).to eq(200)
    end

    it "convert amount to btc for invalid currency" do
      get :tobtc, currency: "USDD", amount: "22"
      expect(response.status).to eq(422)
    end

    it "convert amount to btc for invalid amount" do
      get :tobtc, currency: "USD", amount: "22d"
      expect(response.status).to eq(422)
    end
  end

  describe "GET status" do
    it "list of transactions with 10 limit" do

    end
  end

  describe "GET currency_codes" do
    it "all currency codes" do
      get :currency_codes
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)['currency_code']).to eq(BitcoinPaymentGateway::Application.config.blockchain['currency_code'])
    end
  end
end