require "rails_helper"
require 'byebug'

RSpec.describe Api::UsersController, :type => :controller do

  describe "POST exist" do
    it "check email exist" do
      # Request exist action with email parameter
      post :exist, email: User.last.email
      # Check response body
      expect(response.body).to eq("true")
    end

    it "check email doesn't exist" do
      post :exist, email: "dfgdfg"
      expect(response.body).to eq("false")
    end
  end

  describe "POST resend_confirmation" do
    it "email already verified" do
      post :resend_confirmation, email: User.verified.last.email
      expect(JSON.parse(response.body)['error']).to eq("Email already verified.")
      # check response status
      expect(response.status).to eq(422)
    end

    it "email doesn't exist" do
      post :resend_confirmation, email: "dfgdfg"
      expect(JSON.parse(response.body)['error']).to eq("Email doesn\'t exist.")
      expect(response.status).to eq(404)
    end

    it "verification email sent." do
      email = User.unverified.last.email
      post :resend_confirmation, email: email
      expect(JSON.parse(response.body)['email']).to eq(email)
      expect(response.status).to eq(200)
    end
  end

  describe "POST confirmation" do
    # it "valid token" do
    #   user = User.unverified.first
    #   post :confirmation, confirmation_token: user.confirmation_token, format: "json"
    #   expect(response.status).to eq(200)
    #   expect(response.body).to eq("true")
    # end

    it "invalid token" do
      post :confirmation, confirmation_token: "JHghsdgfjhsdgfh4hjgjh34jhgthj34gjhgf3j4h"
      expect(response.status).to eq(422)
    end
  end

  describe "GET verify_token" do
    it "valid user authentication token" do
      user = User.verified.last
      get :verify_token, {user_email: user.email, user_token: user.authentication_token, format: "json"}
      expect(response.status).to eq(200)
      expect(assigns(:merchant)).to eq(user)
    end

    it "invalid user authentication token" do
      get :verify_token, {user_email: "sdfsdfs@gdfgdf.com", user_token: "FGDSFJKHY328HD87FY734H78", format: "json"}
      expect(response.status).to eq(401)
    end
  end

  describe "GET details" do
    it "should return details of user with valid auth token" do
      user = User.verified.last
      get :details, {user_email: user.email, user_token: user.authentication_token, format: "json"}
      expect(assigns(:merchant)).to eq(user)
    end

    it "should not return details of user with invalid auth token" do
     get :details, {user_email: "sdfsdfs@gdfgdf.com", user_token: "FGDSFJKHY328HD87FY734H78", format: "json"}
      expect(response.status).to eq(401)
    end
  end

end