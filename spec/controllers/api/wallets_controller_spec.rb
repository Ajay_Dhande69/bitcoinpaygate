require "rails_helper"
require 'byebug'

RSpec.describe Api::WalletsController, :type => :controller do

  describe "GET balance" do
    it "check balance" do
      user = User.verified.last
      # Request exist action with email parameter
      get :balance, {user_email: user.email, user_token: user.authentication_token, format: "json"}
      # Check response body
      expect(response.body).to match(/\A[\d]+[\.]{0,1}[\d]*\Z/)
      # verify status code
      expect(response.status).to eq(200)
    end

    it "check balance with invalid authentication token" do
      # Request exist action with email parameter
      get :balance, {user_email: "sdfsdfs@gdfgdf.com", user_token: "FGDSFJKHY328HD87FY734H78", format: "json"}
      # verify status code
      expect(response.status).to eq(401)
    end
  end

  ##--
  # Created By: Sneha Wangle
  # Created On: 04/06/15
  # Purpose: To show the wallet details
  ##++
  describe "GET wallet details" do
    it "get wallet details of current user" do
      user = User.verified.last
      # Request exist action with email parameter
      get :index, {user_email: user.email, user_token: user.authentication_token, format: "json"}
      # Check response body arse in json format
      result = JSON.parse(response.body)
      # Verify result
      expect(result["id"]).to eq(user.wallet.id)
      # verify status code
      expect(response.status).to eq(200)
    end

    it "get wallet details with invalid authentication token" do
      # Request exist action with email parameter
      get :index, {user_email: "sdfsdfs@gdfgdf.com", user_token: "FGDSFJKHY328HD87FY734H78", format: "json"}
      # verify status code
      expect(response.status).to eq(401)
      # 
    end
  end

  ##--
  # Created By: Sneha Wangle
  # Created On: 04/06/15
  # Purpose: To show the current rate of BTC
  ##++
  describe "GET currency_rate" do
    it "get current BTC rate from blockchain API" do
      # Request exist action
      get :currency_rate
      # Check response body arse in json format
      result = JSON.parse(response.body)
      # Verify result
      expect(result.present?).to eq(true)
      # verify status code
      expect(response.status).to eq(200)
      # Check response body
      # expect(response.body).to match(/\A[\d]+[\.]{0,1}[\d]*\Z/)
    end
  end
end