require "rails_helper"
require 'byebug'

RSpec.describe Users::RegistrationsController, :type => :controller do
  render_views
  before(:each) do 
    setup_controller_for_warden
    request.env["devise.mapping"] = Devise.mappings[:user]
  end

  registration_test_data = [
    # invalid first name (characters)
    {
      first_name: "dfgd45345",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "first_name"
    },
    # invalid first name (characters)
    {
      first_name: "jhjsjk$@sdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "first_name"
    },
    # invalid last name (characters)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgd453fg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "last_name"
    },
    # invalid last name (characters)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdg{}dfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "last_name"
    },
    # invalid email address (format)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewed",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "email"
    },
    # invalid email address (format)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "email"
    },
    # invalid password (length)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "654654",
      password_confirmation: "654654",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "password"
    },
    # invalid password confirmation (mismatch)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894061230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "password_confirmation"
    },
    # invalid wallet password (length)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "789456",
      wallet_password_confirmation: "789456",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "wallet_password"
    },
    # invalid wallet password confirmation (mismatch)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561030",
      phone_no: "45845454",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "wallet_password_confirmation"
    },
    # invalid phone no (digit)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "3453fdfffd",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "phone_no"
    },
    # invalid phone no (length)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "4584",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "phone_no"
    },
    # invalid phone no (length)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "458454543453453453453434",
      notification_url: "http://example.com",
      tax_id_no: "56456",
      invalid: "phone_no"
    },
    # invalid notification url (format)
    {
      first_name: "jhjsjksdhd",
      last_name: "fdfdgdfg",
      email: "gdfgdfsgfdgwewedfs@yopmail.com",
      password: "7894561230",
      password_confirmation: "7894561230",
      wallet_password: "7894561230",
      wallet_password_confirmation: "7894561230",
      phone_no: "45845454345",
      notification_url: "example.com",
      tax_id_no: "56456",
      invalid: "notification_url"
    }
  ]

  registration_valid_data = {
    first_name: "jhjsjksdhd",
    last_name: "fdfdgdfg",
    email: "#{(0...8).map { (65 + rand(26)).chr }.join}@yopmail.com",
    password: "7894561230",
    password_confirmation: "7894561230",
    wallet_password: "7894561230",
    wallet_password_confirmation: "7894561230",
    phone_no: "45845454345",
    notification_url: "https://example.com",
    tax_id_no: "56456"
  }

  describe "GET create" do
    it "all field blank" do
      post :create
      expect(response.status).to eq(422)
    end

    registration_test_data.each do |data|
      it "invalid #{data[:invalid]}" do
        post :create, data
        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)["errors"][data[:invalid]].class).to eq(Array)
      end
    end

    it "valid registration data" do
      post :create, registration_valid_data.merge({format: "json"})
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["errors"]).to eq(nil)
    end
  end
end