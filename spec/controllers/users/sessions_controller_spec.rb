require "rails_helper"
require 'byebug'

RSpec.describe Users::SessionsController, :type => :controller do
  # To render views for rsponse body testing
  render_views

  # Need to call for devise controller testing
  before(:each) do 
    setup_controller_for_warden
    request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "POST create" do
    it "should not authenticate with invalid credential" do
      # request action with invalid credential
      post :create, { user: { email: "sdfsdfsdfhf@yopmail.com", password: "uihfiuhdfhfjsdh" } }
      expect(response.status).to eq(401)
    end

    it "should authenticate with valid credential" do
      # Get valid credential from database
      user = User.verified.last
      # request action with valid credential
      post :create, {user:{email: user.email, password: "7894561230"}, format: "json"}
      expect(response.status).to eq(200)
      # prepare correct json response
      correct_response = {"email" => user.email, "authentication_token" => user.authentication_token}
      expect(JSON.parse(response.body)["user"]).to eq(correct_response)
    end
  end

end