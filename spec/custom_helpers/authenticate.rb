# method to fetch an verified user detail and prepare login parameter to get login
def valid_auth_parameters
  # Fetch user from database
  user = User.verified.last
  # wrap credential
  {user_email: user.email, user_token: user.authentication_token, format: "json"}
end

# prepare invalid login parameter to get login
def invalid_auth_parameters
  {user_email: "sdfsdfs@gdfgdf.com", user_token: "FGDSFJKHY328HD87FY734H78", format: "json"}
end