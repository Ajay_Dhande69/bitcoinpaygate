def colorize(text, color_code)
  "\033[#{color_code}m#{text}\033[0m"
end

def red(text); colorize(text, "31"); end
def green(text); colorize(text, "32"); end