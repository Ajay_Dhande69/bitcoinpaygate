require 'spec_helper'
require 'byebug'

# Created by:- Sneha Wangle
# Created on:- 14/05/2015
# Purpose:- To check transaction model methods
describe Transaction do

  context "to btc" do

    # To check amount in btc 
    it "returns converted value of currency to btc" do
      currency_code = 'USD' 
      amount = 23
      transaction = Transaction.currency_to_btc(currency_code, amount)
      expect(transaction.present?).to eq(true)
    end
    
    it "to check invalid currency code for currency to btc" do
      currency_code = 'UqSD' 
      amount = 23 
      begin
        transaction = Transaction.currency_to_btc(currency_code, amount).message
      rescue Exception => e
        expect(e.message).to match("500 Internal Server Error")
      end
    end

    it "to check validation of amount for currency to btc" do
      currency_code = 'USD' 
      amount = "qww2" 
      begin
        transaction = Transaction.currency_to_btc(currency_code, amount).message
      rescue Exception => e
        expect(e.message).to match("500 Internal Server Error")
      end
    end
  end
  
  context "to btc with fee" do 

  # To check amount in btc with network fees
    it "returns converted value of currency to btc with network fees" do
      currency_code = 'USD' 
      amount = 23    
      transaction = Transaction.currency_to_btc_with_fee(currency_code, amount)
      expect(transaction.present?).to eq(true)
    end

    it "to check validation of currency code for currency to btc with network fees" do
      currency_code = 'UqSD' 
      amount = 23 
      begin
        transaction = Transaction.currency_to_btc_with_fee(currency_code, amount).message
      rescue Exception => e
        expect(e.message).to match("500 Internal Server Error")
      end
    end

    it "to check validation of currency code for currency to btc with network fees" do
      currency_code = 'USD' 
      amount = "qww2" 
      begin
        transaction = Transaction.currency_to_btc_with_fee(currency_code, amount).message
      rescue Exception => e
        expect(e.message).to match("500 Internal Server Error")
      end
    end
  end


# To generate QR code
  it "to show generated QR code" do
    merchant = User.find_by_email('sneha.wangle@cryptextechnologies.com')
    expect(merchant).to_not eq(nil)
    wallet_address = merchant.wallet.wallet_address
    amount_in_btc = 2.03    
    qr_code = Transaction.qr_code(wallet_address, amount_in_btc)
    expect(qr_code).to eq(true)
  end
end