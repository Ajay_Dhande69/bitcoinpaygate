require 'spec_helper'
require 'byebug'

describe User do
  it "has api_token and secret_key" do
    user = User.new
    expect(user.api_token.present?).to eq(true)
    expect(user.secret_key.present?).to eq(true)
  end

  it "returns a contact's full name as a string" do
    user = User.last
    expect(user.full_name).to eq("#{user.first_name} #{user.last_name || ''}")
  end

  it "returns a user object by keys(api_token & secret_key)" do
    merchant = User.find_by_email('sneha.wangle@cryptextechnologies.com')
    expect(merchant).to_not eq(nil)
    user = User.find_by_keys(merchant.api_token, merchant.secret_key)
    expect(user).to eq(merchant)
  end

  it "returns transaction_id" do
    merchant = User.find_by_email('sneha.wangle@cryptextechnologies.com')
    expect(merchant).to_not eq(nil)
    hash = {api_token: merchant.api_token, secret_key: merchant.secret_key, amount: 23,currency: 'USD', return_url: 'http://sdfsdfsdfsdfsd.com', cancel_url: 'http://dfdsfsdfsdfsdfsdfsdf.com'}
    transaction = merchant.initTransaction(hash[:amount], hash[:currency], hash[:return_url], hash[:cancel_url])
    expect(transaction.new_record?).to eq(false)
  end

  it "checks error handling for invalid currency" do
    merchant = User.find_by_email('sneha.wangle@cryptextechnologies.com')
    expect(merchant).to_not eq(nil)
    hash = {api_token: merchant.api_token, secret_key: merchant.secret_key, amount: 23, return_url: 'http://sdfsdfsdfsdfsd.com', cancel_url: 'http://dfdsfsdfsdfsdfsdfsdf.com'}
    transaction = merchant.initTransaction(hash[:amount], hash[:currency], hash[:return_url], hash[:cancel_url])
    expect(transaction.new_record?).to eq(true)
    expect(transaction.errors.messages.present?).to eq(true)
    expect(transaction.errors.messages.keys.include?(:currency)).to eq(true)
    hash = {api_token: merchant.api_token, secret_key: merchant.secret_key, amount: 23,currency: 'RS', return_url: 'http://sdfsdfsdfsdfsd.com', cancel_url: 'http://dfdsfsdfsdfsdfsdfsdf.com'}
    transaction = merchant.initTransaction(hash[:amount], hash[:currency], hash[:return_url], hash[:cancel_url])
    expect(transaction.new_record?).to eq(true)
    expect(transaction.errors.messages.present?).to eq(true)
    expect(transaction.errors.messages.keys.include?(:currency)).to eq(true)
  end

  it "checks error handling for invalid amount" do
    merchant = User.find_by_email('sneha.wangle@cryptextechnologies.com')
    expect(merchant).to_not eq(nil)
    hash = {api_token: merchant.api_token, secret_key: merchant.secret_key, currency: 'USD', return_url: 'http://sdfsdfsdfsdfsd.com', cancel_url: 'http://dfdsfsdfsdfsdfsdfsdf.com'}
    transaction = merchant.initTransaction(hash[:amount], hash[:currency], hash[:return_url], hash[:cancel_url])
    expect(transaction.new_record?).to eq(true)
    expect(transaction.errors.messages.present?).to eq(true)
    expect(transaction.errors.messages.keys.include?(:amount)).to eq(true)
    hash = {api_token: merchant.api_token, secret_key: merchant.secret_key, amount: 'fsdf',currency: 'USD', return_url: 'http://sdfsdfsdfsdfsd.com', cancel_url: 'http://dfdsfsdfsdfsdfsdfsdf.com'}
    transaction = merchant.initTransaction(hash[:amount], hash[:currency], hash[:return_url], hash[:cancel_url])
    expect(transaction.new_record?).to eq(true)
    expect(transaction.errors.messages.present?).to eq(true)
    expect(transaction.errors.messages.keys.include?(:amount)).to eq(true)
  end

  it "checks error handling for invalid return_url and cancel_url" do
    merchant = User.find_by_email('sneha.wangle@cryptextechnologies.com')
    expect(merchant).to_not eq(nil)
    hash = {api_token: merchant.api_token, secret_key: merchant.secret_key, amount: 23,currency: 'USD'}
    transaction = merchant.initTransaction(hash[:amount], hash[:currency], hash[:return_url], hash[:cancel_url])
    expect(transaction.new_record?).to eq(true)
    expect(transaction.errors.messages.present?).to eq(true)
    expect(transaction.errors.messages.keys.include?(:return_url)).to eq(true)
    expect(transaction.errors.messages.keys.include?(:cancel_url)).to eq(true)
    hash = {api_token: merchant.api_token, secret_key: merchant.secret_key, amount: 23,currency: 'USD', return_url: 'httsdfsdfsdfsdfsd.com', cancel_url: 'httpdfdsfsdfsdfsdfsdfsdf.com'}
    transaction = merchant.initTransaction(hash[:amount], hash[:currency], hash[:return_url], hash[:cancel_url])
    expect(transaction.new_record?).to eq(true)
    expect(transaction.errors.messages.present?).to eq(true)
    expect(transaction.errors.messages.keys.include?(:return_url)).to eq(true)
    expect(transaction.errors.messages.keys.include?(:cancel_url)).to eq(true)
  end
end