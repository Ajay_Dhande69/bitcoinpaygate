require 'spec_helper'
require 'byebug'

# Created by:- Sneha Wangle
# Created on:- 13/05/2015
# Purpose:- To check wallet balance
describe Wallet do

# To check wallet creation
  it "to create wallet for merchant with valid parameters" do
    wallet_password = "7894561230"
    wallet = Wallet.create_wallet(wallet_password)
    expect(wallet.present?).to eq(true)
  end

  it "to check invalid wallet_password length for create wallet of merchant" do
    wallet_password = "7894"
    begin
      wallet = Wallet.create_wallet(wallet_password).message
    rescue Exception => e
      expect(e.message).to match("Password must be at least 10 characters")
    end
  end

# Created by:- Sneha Wangle
# Created on:- 06/05/2015
# To check wallet balance of merchant
  it "to check wallet balance" do
    merchant = User.find_by_email('sneha.wangle@cryptextechnologies.com')
    expect(merchant).to_not eq(nil)
    wallet_address = merchant.wallet.wallet_address
    wallet = Wallet.wallet_balance(wallet_address)
    expect(wallet.present?).to eq(true)
  end

# satoshi in BTC
  it "satoshi to btc" do
    btc = 200 * 0.0001
  	value = 200
  	wallet = Wallet.satoshi_to_btc(value)
    expect(wallet.present?).to eq(true)
  end
  
  ##--
  # Created By: Sneha Wangle
  # Created On: 04/06/15
  # Purpose: To show the current rate of BTC
  ##++
  it "Current rate of BTC using blockchain API" do
    # To call current rate method
    rate = Wallet.current_rate
    # Verify rate
    expect(rate.present?).to eq(true)
  end
end